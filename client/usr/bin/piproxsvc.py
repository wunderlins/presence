#!/usr/bin/env python3
# -*- mode: python3; indent-tabs-mode: nil; tab-width: 4 -*-
import os, sys
exec_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(exec_dir + "/../..") # change working directory to prefix
sys.path.insert(0, exec_dir + '/../lib')
sys.path.insert(0, exec_dir + '/../lib/piprox')

import pcprox
from time import sleep
from enum import IntEnum
import subprocess

class ExitCodes(IntEnum):
    OK = 0
    BADREAD = 1
    BADDEVICE = 2
    KILLSIG = 3
    UNHANDLED = 4

dev: pcprox.PcProx = None
debug: bool = False

def handler(signum, frame):
    logging.info('Caught signal '+str(signum))

    ret = ExitCodes.KILLSIG
    if signum == signal.SIGTERM or signum == signal.SIGINT:
        ret = ExitCodes.OK
    
    close(ret)
    sys.exit(ret)

def open():
    """ open the hid device with hidapi 
    
    set the led to gree to indicate operational state """
    logging.info("Opening device")
    global dev
    if dev == None:
        try:
            dev = pcprox.open_pcprox(debug=debug)
        except:
            logging.error("Failed to open reader device")
            ret = ExitCodes.BADDEVICE
            close(ret)
            sys.exit(ret)
    return dev

def close(ret=ExitCodes.UNHANDLED):
    """ close the hid device handle

    try to turn off both leds to inidcate that the reader is not operational anymore"""
    logging.info("Closing device")
    os.environ["EXIT_CODE"] = str(int(ret))
    return_code = run_script("shutdown") # run shutdown script
    try:
        led_color()
    except:
        pass

    if dev != None:
        try:
            dev.close()
        except:
            pass

def led_color(red=False, green=False):
    global dev
    """ set the state of an led """
    led_config: pcprox.Configuration = dev.get_config()
    led_config.iRedLEDState = red
    led_config.iGrnLEDState = green
    led_config.set_config(dev, [2])
    dev.end_config()
    #print((red, green))

def blink(state = False, interval=None, duration=.8):
    """blink green when ok, blink red when state is False """
    if interval == None: interval = float(cfg["device"]["led_interval"])
    if duration == None: duration = float(cfg["device"]["led_repeat"])
    #print(duration); print(interval)

    steps = range(int(duration / interval))
    for i in steps:
        if i % 2 == 0:
            led_color(red=(not state), green=state)
        else:
            led_color(red=False, green=False)
        sleep(interval)

def run_script(event):
    script = cfg["events"][event].strip()
    if script == "":
        logger.debug((-1, "No script configured for " + event))
        return -1

    try:
        proc = subprocess.Popen(script, 
                                env=os.environ.copy(), 
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.STDOUT)
        out, err = proc.communicate()
        return_code = proc.returncode
        logger.info((return_code, out.decode("utf-8")))
    except Exception as ex:
        logger.error((-2, "Failed to execute script "+script+": "+str(ex)+", "+type(ex)+""))
        return -2

    return return_code, out.decode("utf-8")

def main():
    """busy wait loop, reading for cardreader input

    This is the main loop. it reads input from the card reader.

    Multiple passes of a card is prevented by remebering the last card number 
    and waiting for N seconds until the same card is accepted again.
    """
    logging.info("Starting service")

    block_for_new_card = 50 # deci seconds (S * 10)
    beep = False
    try:
        if cfg["device"]["beep"]:
            v = cfg["device"]["beep"].strip().lower()
            if v == "on" or v == "1" or v == "true":
                beep = True
    except: pass

    open() # connect to device

    # initialize device, set LED default state to read ans save it to 
    # EEPROM. This way we make sure that if the service is not running, 
    # the LED state goes to RED
    dev_cfg = dev.get_config()
    dev_cfg.bHaltKBSnd = True # Disable sending keystrokes, as we want direct control
    # Turn on the red LED
    dev_cfg.iRedLEDState = True
    dev_cfg.iGrnLEDState = False
    dev_cfg.bAppCtrlsLED = True # Tells pcProx that the LEDs are under application control
    dev_cfg.iBeeperState = beep
    dev_cfg.bBeepID = beep
    dev_cfg.set_config(dev) # Send the updated configuration to the device
    dev.save_config(0x7) # write defaults to eeprom
    dev.end_config() # Exit configuration mode

    # enable green led when we are operational
    led_color(green=True)
    os.environ["HOST"] = cfg["service"]["backend_host_name"].strip()
    os.environ["PORT"] = cfg["service"]["backend_port"].strip()

    # busy wait for data
    last_tag = None
    x = 0
    while True:
        try:
            tag = dev.get_tag()
        except:
            logging.error("Failed to read from reader")
            close()
            sys.exit(ExitCodes.BADREAD)

        if tag != None and last_tag != tag:
            last_tag = tag
            hexbe = pcprox._format_hex(tag[0]).replace(" ", "").upper()
            rev = [tag[0][3], tag[0][2], tag[0][1], tag[0][0]]
            hexle = pcprox._format_hex(rev).replace(" ", "").upper()
            logging.info("badge: "+hexbe+" "+hexle)

            # log transaction to database
            sql = "INSERT INTO log (badge_id) VALUES ('"+hexbe+"')"
            dbconn.execute(sql)
            dbconn.commit()

            # get last id
            sql = "SELECT MAX(id) FROM log"
            last_id = int(dbconn.cursor().execute(sql).fetchone()[0])
            
            # export bade ID
            if cfg["device"]["endianness"].strip() == "LE":
                os.environ["BADGE_UID"] = hexle
            else:
                os.environ["BADGE_UID"] = hexbe

            # run event script
            return_code, txt = run_script("transmit")

            if return_code == 0:
                blink(True)
            else:
                blink(False)

            # update log in database
            txt = txt.replace("'", "''") # escape single quotes
            sql = "update log set ret="+str(return_code)+", ret_string='"+txt+"' WHERE id="+str(last_id)
            dbconn.execute(sql)
            dbconn.commit()

            led_color(green=True)

        sleep(.1)
        x += 1
        if x > block_for_new_card: # clear last card after N*10 seconds
            last_tag = None
            x = 0

    logging.info("Stopping service after unhandled error ...")
    close()
    sys.exit(ExitCodes.UNHANDLED)

if __name__ == "__main__":
    import argparse
    import signal, os, sys
    import logging
    from logging import handlers
    import configparser
    import sqlite3

    cfg = configparser.ConfigParser()
    cfg.read(exec_dir + '/../../etc/piprox.ini')
    #print(config)

    filename = cfg["service"]["log_file"]

    log_handler = handlers.TimedRotatingFileHandler(filename, when=cfg["service"]["log_rotate"])
    log_formatter = logging.Formatter(
        fmt='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    log_handler.setFormatter(log_formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO)

    # Set the signal handler (traps)
    signal.signal(signal.SIGALRM, handler)
    #signal.signal(signal.SIGKILL, handler)
    signal.signal(signal.SIGBUS, handler)
    signal.signal(signal.SIGILL, handler)
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGPIPE, handler)
    signal.signal(signal.SIGTERM, handler)

    # parse arguments
    parser = argparse.ArgumentParser(
        description='Test program for pcprox which reads a card in the field')

    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug traces')

    options = parser.parse_args()
    debug = options.debug

    # run startup script
    return_code = run_script("startup")

    # start the transaction log database
    dbconn = None
    db_file = exec_dir + "/../../var/piprox/transaction_log.db"
    if not os.path.exists(db_file):
        dbconn = sqlite3.connect(db_file)
        # create schema
        cur = dbconn.cursor()
        cur.execute("""
            CREATE TABLE log (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                tst TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                badge_id TEXT,
                ret NUMBER,
                ret_string TEXT
            )
        """)
        dbconn.commit()
    else:
        dbconn = sqlite3.connect(db_file)

    main()
