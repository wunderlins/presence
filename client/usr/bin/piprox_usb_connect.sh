#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
logfile=$SCRIPT_DIR/../../var/log/piprox_usb_connect.log

# debug outputto file
function log() {
    dt=$(date +"%Y-%m-%d %H:%M:%S")
    echo "${dt} ${1}" >> $logfile
}

#log "${ACTION}"

if [ "${ACTION}" = bind -a -d "/sys${DEVPATH}" ]; then
    # check if we have an inputNN device
    #echo "${DEVPATH}" | grep '0C27:3BFA\.[A-Z0-9]\+$' - > /dev/null 2>&1
    #log "${ACTION} ${SUBSYSTEM} ${DRIVER}"
    if [ "${SUBSYSTEM}" = usb -a "${DRIVER}" = usb ]; then
        #log "`env`"
        #log ""

        # TODO: start service if it is not already running
        #echo "${DEVPATH}" >> $logfile
        #echo "" >> $logfile
        #env     >> $logfile
        log "connected /sys${DEVPATH}"
        systemctl restart piprox
    fi
fi