# Fortbildungslog ANA

- [Alte Lesegeräte](https://it/display/KA/Badgeleser+in+den+Hoersaelen)
- [Fortbildungslog (source)](https://gitlab.uhbs.ch/klinische-informationssysteme/raspi/-/blob/master/anaintra/fortbildunglog/index.php)
- [fortbildung.uhbs.ch]([smb://ictmedwebwp02/d$/Websites_dri/fortbildunglog/](https://gitlab.uhbs.ch/administrative-l-sungen/fortbildung_log))

## Neue architektur

```plantuml
@startuml
!theme _none_

boundary "Badge Leser" as BADGE
box "fortbildung.uhbs.ch" #LightBlue
	participant "Web Service" as WS
	database "MA DB\nbadgedb" as BADGEDB
end box
database "ISOP"
database "Kalender" as CAL
database "Logbook RET" as LOGRET
database Presence as PRESENCE


BADGE -> WS : Badge#, Device-Name

alt ANA 
	WS -> CAL : Location, Datetime
	WS <- CAL : Event
	WS -> PRESENCE : Legic© Nummer
	note right: hier wird die\nLegicUID übergeben
	WS <- PRESENCE : sAMAccountName (AD Username)
	WS -> ISOP : sAMAccountName
	WS <- ISOP : ISOP Code (3 Buchstaben Code für OP Personal)

	alt ANA Ärzte
		WS -> LOGRET : stamp » log
		WS <- LOGRET :
	else ANA Pflege
		WS -> BADGEDB : stamp » log
		WS <- BADGEDB :
	end
else OP Pflege
	WS -> PRESENCE : Hostname, Badge# » stamp » log
	WS <- PRESENCE : 
end

WS -> BADGE : success / error

@enduml
```

### Sories

- Presence: exponiere Webservice zum konvertieren von `LegicUID` nach `sAMAccountNAme`
- ISOP: WEB-Service zum Konvertieren von `sAMAccountName` nach `ISOP Code`
- batchleser.uhbs.ch: bestehende dabge logik übererbeiten (siehe sequenz Diagram)


## Status Quo

```plantuml
@startuml
!theme _none_

boundary "Badge Leser" as BADGE
box "fortbildung.uhbs.ch" #LightBlue
	participant "Web Service" as WS
	database "MA DB badgedb" as BADGEDB
end box
database "MA DB planoaa" as PLANOAA
database "Kalender" as CAL
database "Logbook RET" as LOGRET

BADGE -> WS : Badge#, Device-Name
WS -> CAL : Location, Datetime
WS <- CAL : Event

alt ANA Ärzte
	WS -> PLANOAA : Hostname, Badge#
	note right: hier wird die Nedap\nNummer übergeben
	WS <- PLANOAA : Code (3 letter person code from ISOP)
	WS -> LOGRET : stamp » log
	WS <- LOGRET :
else ANA Pflege
	WS -> BADGEDB : Hostname, Badge#
	note right: hier wird die Nedap\nNummer übergeben
	WS <- BADGEDB : Code (3 letter person code from ISOP)
	WS -> BADGEDB : stamp » log
	WS <- BADGEDB :
end

WS -> BADGE : success / error

@enduml
```

1. `/root/processbatchd.sh` USB «nedap id»
2. «nedap id» HTTP -> `batchleser.uhbs.ch`
   1. `batchleser.uhbs.ch` -> MYSQL kürzel aus ret
   2. `batchleser.uhbs.ch` -> WGET Termin aus kalender
   3. `batchleser.uhbs.ch` -> log to RET

```plantuml
@startuml
!theme cerulean

component [1. Stempeluhr\nBadge Leser] as RASPI
component [2. batchleser.uhbs.ch] as BATCH
component [3. planoaa] as PLANOAA
component [4. Kalender] as CAL
component [5. Resident\nEvaluation\nTool] as RET

RASPI <--> BATCH
BATCH <-left-> PLANOAA: Nedapp ID
BATCH <-down- CAL: Finde\nVeranstaltung
BATCH -right-> RET: Gutschrift\nFortbildung

@enduml
```

```bash
#!/usr/bin/env bash

pidfile="/var/run/processbatchd.pid"
logfile="$scriptpath/processbatchd.log"
green=17
red=22
mailaddr="MQ_ANA_IT_Cron@usb.ch"
dropoffurl="http://batchleser.uhbs.ch/index.php?combi="
batchleser="&batchleser="
```

### processbatchd.sh

```bash
#!/usr/bin/env bash

# processbatchd.sh is a daemon for prcessing the input of nedap batchreaders via a python script.
# Dependencies: systemd, Bash 4, Python2, evdev lib for Python, usleep and a Raspberry Pi to run on.
# The main input is a string of numbers from the read_input.py
# The output is a URL that contains the input from the read_input.py to be processed on the recieving server.
# Feedback from the Server get logged in to the logfile and depending on the status code a red or green LED blinks once.
# If an undesired status code > 0 is returned form the server a mail gets send and the red LED blinks once.

# Dominik Riva 2013.08.16

(

scriptpath=`readlink --canonicalize $(dirname $0)`
. $scriptpath/config.sh
host=`hostname`
if [[ -f $pidfile ]]; then
        pgrep -F $pidfile >/dev/null
        if [[ $? == 0 ]]; then
                echo "Already running"
                exit 1
        fi
fi

echo $BASHPID > $pidfile

# use raspi gpio driver to initilize the gpio pins for red and green LED
. $scriptpath/gpio.sh
init $red 2> /dev/null
init $green 2> /dev/null

# function to blink led. usleep source code can be found in data/usleep.c
blinkled () {
        i=0
        while [ $i -lt $1 ]
        do
                on $2
                $scriptpath/usleep 250000
                off $2
                $scriptpath/usleep 250000
                i=$(( $i + 1 ))
        done
}

# trap shutdown handler
cleanup () {
        blinkled 5 $red
        rm -f $pidfile
        pkill -P $BASHPID
        exit 255
}

trap cleanup INT QUIT TERM

blinkled 5 $green

# main loop
while true
do
        # coproc gets used because the other alternatives block trap
        coproc /usr/bin/python2 $scriptpath/read_input.py /dev/input/by-id/usb-NEDAP_N.V._Desktop_reader_*-event-kbd
        read -ru ${COPROC[0]} combi
        wait ${!}
        if [ $? -gt 0 ] ;then
                # the coproc exited with an error
                blinkled 3 $red
                sleep 1
                continue
        else
                # send combi number to server
                requesturl="$dropoffurl$combi$batchleser$host"
                response=`wget -q -O - $requesturl`
                # write to logfile
                datetime=`date "+%Y-%m-%d%%20%H:%M:%S"`
                replay="$requesturl&datetime=$datetime"
                echo "$datetime $combi $response $replay" >> $logfile
                # signal sucess by LED
                if [[ $response =~ ^0 ]];then
                        on $green
                        $scriptpath/usleep 1000000
                        off $green
                        if [[ $response != *0 ]];then
                        # respons was a 0 but something followed it so we send a mail to report the message 
                                message="Hallo IT-Team

Stempeluhr Schulungen hat einen Fehler festgestellt.
Moeglicherweise ist die Schulung kurzfristig verschoben worden.

Datum und Uhrzeit: $datetime
Combi Nr.: $combi
Fehlermeldung: $response

$replay

lg
$host"
                                echo "$message" | tr -d '\15\32' | iconv -c -f ISO8859-15 -t ascii - | mail -s "$host Error" -r "${host}@usb.ch" $mailaddr
                        fi
                # signal failure by LED and send mail
                else
                        on $red
                        $scriptpath/usleep 1000000
                        off $red
                        message="Hallo IT-Team

Stempeluhr Schulungen hat einen Fehler festgestellt

Datum und Uhrzeit: $datetime
Combi Nr.: $combi
Fehlermeldung: $response

$replay

lg
$host"
                        echo "$message" | tr -d '\15\32' | iconv -c -f ISO8859-15 -t ascii - | mail -s "$host Error" -r "${host}@usb.ch" $mailaddr
                fi
        fi
done

)&
```

### read_input.py

```python
#!/usr/bin/env python2

import string

from evdev import InputDevice
from select import select
import sys

# key mapping, keyboard codes to ascii characters
keys = "X^1234567890XXXXqwertzuiopXXXXasdfghjklXXXXXyxcvbnmXXXXXXXXXXXXXXXXXXXXXXX"
dev = InputDevice(sys.argv[1])

charbuffer = ""

# read input character by character, stuff it into the charbuffer variable until we reach the end of the string. EOS is "\n" which results to 'X'
# print string without "\n"
while True:
        r,w,x = select([dev], [], [])
        for event in dev.read():
                if event.type==1 and event.value==1:
                        #print( keys[ event.code ] )
                        if keys[event.code] == "X":
                                print charbuffer
                                sys.exit(0)
                        else:
                                charbuffer += str(keys[event.code])

```

### index.php

```php
<?php

$room = "Seminarraum";
$block = "Blockkurs";
$vor = 30;
$nach = 15;

include("../stellenplanv1.0.1a/tool/dbconfig.php");
include("../stellenplanv1.0.1a/include/adodb/adodb.inc.php");
//$vonRelPfad = '../RETv2.1.7a/';
//require_once($vonRelPfad . 'start/PHPStart.php');
//require_once('../stellenplanv1.0.1a/include/ISM.class.php');
ini_set("reporting_level", E_ALL);
// restore error handling
restore_error_handler();
restore_exception_handler();
error_reporting(E_ALL);

// combi nr entegen nehmen
if ($_GET["combi"] == "") {
	echo 4;
	exit;
}

function connectToRetDB() {
	global $retDBName;
	global $retDBUser;
	global $retDBPW;
	global $retDbHost;
	
	return connectToDB($retDBName, $retDBUser, $retDBPW, $retDbHost);
}

function connectToPlanDB() {
	global $planDBName;
	global $planDBUser;
	global $planDBPW;
	global $planDbHost;
	
	return connectToDB($planDBName, $planDBUser, $planDBPW, $planDbHost);
}

function connectToDB($db, $user, $pw, $host="localhost") {
	global $dbconn;
	unset($dbconn);
	
	$dbconn = NewADOConnection('mysqli');
	$dbconn->port = '3306';
	if (!$dbconn->PConnect($host, $user, $pw, $db)) {
		return false;
	} else {
		$GLOBALS['dbconn'] = $dbconn;
		return true;
	}
}

// kürzel lookup in planoaa gem. combi nr
  // fehler echo 1

if (!connectToPlanDB()) {
	echo 1;
	exit;
}

$sql = "SELECT kuerzel FROM personal WHERE combi = " . $_GET["combi"];
if ($dbconn->Execute($sql) === false) {
	echo 1;
	exit;
} else {
	$rs = $dbconn->Execute($sql);
}
$rs = $rs->GetRows();

if ( empty( $rs )) {
	echo 1;
	exit;
}

$kuerzel = $rs[0]['kuerzel'];

// lookup der veranstaltung
// http://www.anaesthesie.ch/queries/listing.php?leer=1 (als vorlage)
  // fehler echo 2

$timestamp = time();
$datum = date("Y.m.d",$timestamp);
$uhrzeit = date("H:i",$timestamp);
$scounter = 0;

//$datum = "2013.08.21";
//$uhrzeit = "16:50";

/*
naz
Table: meetings
Columns:
ID	int(10) UN PK AI
DATE	varchar(21) 
TIME	varchar(21) 
TOPIC	text 
SPEAKER	text 
Datum	date 
Zeit	varchar(5) 
Ort	varchar(33) 
Referent	varchar(111) 
Herkunft	varchar(111) 
Block	varchar(21) 
Comment	text
*/

$keys = array(
	'ID',
	'DATE',
	'TIME',
	'TOPIC',
	'SPEAKER',
	'Datum',
	'Zeit',
	'Ort',
	'Referent',
	'Herkunft',
	'Block',
	'Comment');

function tominutes ( $hsmins) {
	$fragments = explode(":",$hsmins);
	$minutes = substr($fragments[0], -2) * 60 + substr($fragments[1], 0, 2);
	return $minutes;
}

function check_time ( $timefield, $uhrzeit, $vor, $nach) {
	$difference = tominutes($uhrzeit) - tominutes($timefield);
	$before = tominutes($timefield) - $vor;
	$after = tominutes($timefield) + $nach;
	/*echo $difference . "\n";
	echo $before . "\n";
	echo $after . "\n";
	echo $timefield . "\n";
	echo $uhrzeit . "\n";
	echo tominutes($uhrzeit) . "\n". "\n";
	*/
	if ( tominutes($uhrzeit) >= $before && tominutes($uhrzeit) <= $after) {
		return true;
	}

	return false;
}

if ( file_exists( 'meetings.csv')) {
	$file = fopen('meetings.csv', 'r');
} else {
	echo 2;
	exit;
}

while ( $line = fgetcsv($file, 0, ',','"')) {
	$num = count ($line);

	if ($line[1] != $datum || $line[7] != $room || $line[10] != $block) {
		continue;
	}

	if (check_time($line[2], $uhrzeit, $vor, $nach)) {
		$schulung = array_combine($keys, $line);
		$scounter++;
		//print_r($schulung);
	}
}

fclose($file);

if ($scounter != 1) {
	echo 2;
	exit;
}

// eintrag in ret
  // fehler echo 3
  // fehler echo 4 beim Versuch einer wiederholten eingebe mit selbem Kürzel für den selben Kurs
/*
ret_test1
Table: pf
Columns:
id	int(11) AI
TS	timestamp 
name	varchar(50) 
pw	varchar(33) 
datum	varchar(33) 
TYPE	varchar(55) 
pf	varchar(255) 
refer	varchar(33)
idschulung int(10) 
*/

$keysret = array(
	'id',
	'TS',
	'name',
	'pw',
	'datum',
	'TYPE',
	'pf',
	'refer',
	'idschulung');

$keysret = array_combine($keysret, $keysret);

//print_r($keysret);


/*
Blockkurs
Intern
*/

$typearr = array(
	'' => 'Paper',
	'' => 'Pres',
	'' => 'Congr',
	'Intern' => 'Fall',
	'Blockkurs' => 'Lect',
	'' => 'Div');

$dataarr = array(
	$keysret['TS'] 		=> date("Y-m-d H:i:s",$timestamp),
	$keysret['name'] 	=> $kuerzel,
	$keysret['pw'] 		=> '',
	$keysret['datum'] 	=> $schulung['DATE'],
	$keysret['TYPE'] 	=> $typearr[$schulung['Block']],
	$keysret['pf'] 		=> 'Ort: ' . $schulung['Ort'] . "\nReferent: " . $schulung['SPEAKER'] . "\nBeschreibung: " . $schulung['TOPIC'],
	$keysret['refer'] 	=> $schulung['SPEAKER'],
	$keysret['idschulung'] 	=> $schulung['ID']);

//print_r($dataarr);

// Insert

if (!connectToRetDB()) {
	echo 3;
	exit;
}

$sql = "SELECT * FROM pf WHERE idschulung = '" . $schulung['ID']. "' AND name ='" . $kuerzel . "'";
if ($dbconn->Execute($sql) === false) {
	echo 3;
	exit;
} else {
	$rs = $dbconn->Execute($sql);
}
$rs = $rs->GetRows();
//print_r($rs);

if (empty($rs)) {
	$keylist = '';
	$valuelist = '';
	$count = count($dataarr);
	$i = 1;
	foreach ($dataarr as $key => $value) {
		$key = $dbconn->qstr(trim($key), false);
		$key = substr($key, 1, strlen($key) - 2);
		$keylist .= $key;
		if ($i < $count) {
			$keylist .= ', ';
		}
		$value = $dbconn->qstr(trim($value), false);
		$valuelist .= $value;
		if ($i < $count) {
			$valuelist .= ', ';
		}
		$i++;
	}
	$sql = 'insert into pf (' . $keylist . ') values (' . $valuelist . ')';
	//echo $sql;
	if ($dbconn->Execute($sql) === false) {
		echo 3;
		exit;
	}
	//print_r($dbconn);
} else {
	echo 4;
	exit;
}

// success echo 0
echo 0;
exit;

?>
```

### batchleser.uhbs.ch/index.php

```php
<?php
// normal evocation from regular usage of the batchreaders
//http://anaintra/fortbildunglog/index.php?combi=19297&batchleser=srrasln01
// Admin evocation with a known batchreader and time
//http://anaintra/fortbildunglog/index.php?combi=19297&batchleser=srrasln01&datetime=2014-02-20%2017:30:00
// Admin evocation with a known ort and a time
//http://anaintra/fortbildunglog/index.php?combi=19297&ort=Hoersaal%2005&datetime=2014-02-20%2017:30:00

// Einstellungen

$block = "Blockkurs";
$fall = "Intern";
$befor = 15;
$after = 45;

$message = "";

include("dbconfig.php");
include("include/adodb/adodb.inc.php");
include("include/jsonwrapper/jsonwrapper.php");

// restore error handling
ini_set("reporting_level", E_ALL);
restore_error_handler();
restore_exception_handler();
error_reporting(E_ALL);

function connectToRetDB() {
	global $retDBName;
	global $retDBUser;
	global $retDBPW;
	global $retDbHost;
	
	return connectToDB($retDBName, $retDBUser, $retDBPW, $retDbHost);
}

function connectToPlanDB() {
	global $planDBName;
	global $planDBUser;
	global $planDBPW;
	global $planDbHost;
	
	return connectToDB($planDBName, $planDBUser, $planDBPW, $planDbHost);
}

function connectToBatchleserDB() {
	global $batchleserDBName;
	global $batchleserDBUser;
	global $batchleserDBPW;
	global $batchleserDbHost;
	
	return connectToDB($batchleserDBName, $batchleserDBUser, $batchleserDBPW, $batchleserDbHost);
}

function connectToDB($db, $user, $pw, $host="localhost") {
	global $dbconn;
	unset($dbconn);
	
	$dbconn = NewADOConnection('mysqli');
	$dbconn->port = '3306';
	if (!$dbconn->PConnect($host, $user, $pw, $db)) {
		return false;
	} else {
		$GLOBALS['dbconn'] = $dbconn;
		return true;
	}
}

function getKuerzelFromPlanOAA($combi){
	// connect to DB
	global $planDBName;
	$dbname = $planDBName;
	global $planDbHost;
	$dbhost = $planDbHost;
	if (!connectToPlanDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit(1);
	}
	global $dbconn;
	$sql = "SELECT kuerzel FROM personal WHERE combi = " . $dbconn->qstr(trim($combi), false);
	if ($dbconn->Execute($sql) === false) {
			echo "Selecton of the Kürzel in DB $dbname failed for query:\n";
			print_r($sql);
			exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();

	if ( empty( $rs )) {
		//echo "Kuerzel nicht gefunden in planoaa.personal";
		return false;
	}

	$kuerzel = $rs[0]['kuerzel'];
	return $kuerzel;
}

function getKuerzelFromBatchleser($combi){
	// connect to DB
	global $batchleserDBName;
	$dbname = $batchleserDBName;
	global $batchleserDbHost;
	$dbhost = $batchleserDbHost;
	if (!connectToBatchleserDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit;
	}
	global $dbconn;
	$sql = "SELECT Kuerzel FROM personen WHERE CombiNr = " . $dbconn->qstr(trim($combi), false);
	if ($dbconn->Execute($sql) === false) {
			echo "Selecton of the Kürzel in DB $dbname failed for query:\n";
			print_r($sql);
			exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();

	if ( empty( $rs )) {
		//echo "Kuerzel nicht gefunden in batchleser.personen";
		return false;
	}

	$kuerzel = $rs[0]['Kuerzel'];
	return $kuerzel;
}

function getIdPersFromRet($kuerzel) {
	// connect to DB
	global $retDBName;
	$dbname = $retDBName;
	global $retDbHost;
	$dbhost = $retDbHost;
	if (!connectToRetDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit;
	}
	global $dbconn;
	$sql = "SELECT ID FROM conf_personal WHERE CODE = " . $dbconn->qstr(trim ($kuerzel), false) . " AND INAKTIV != 1";
	if ($dbconn->Execute($sql) === false) {
			echo "Selecton of the ID in DB $dbname failed for query:\n";
			print_r($sql);
			exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();

	if ( empty( $rs )) {
		echo "ID for $kuerzel not found in ret3.conf_personal";
		exit(1);
	}

	$idPerson = $rs[0]['ID'];
	return $idPerson;
}

function findOrt($batchleser){
	// connect to DB
	global $batchleserDBName;
	$dbname = $batchleserDBName;
	global $batchleserDbHost;
	$dbhost = $batchleserDbHost;
	if (!connectToBatchleserDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit(1);
	}
	global $dbconn;
	$sql = 'SELECT Ort FROM orte JOIN leser ON orte.idOrte = leser.idOrt WHERE leser.Name = ' . $dbconn->qstr(trim($batchleser), false);
	if ($dbconn->Execute($sql) === false) {
			echo "Selectin of the ort in DB $dbname failed for query:\n";
			print_r($sql);
			exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();
	
	if ( empty( $rs )) {
		//echo "Ort nicht gefunden in batchleser.leser";
		return false;
	}

	$ort = $rs[0]['Ort'];
	return $ort;
}

function findEvent($ort, $datetime){
	// connect to DB
	global $batchleserDBName;
	$dbname = $batchleserDBName;
	global $batchleserDbHost;
	$dbhost = $batchleserDbHost;
	if (!connectToBatchleserDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit(1);
	}
	global $dbconn;
	global $befor;
	global $after;
	
	global $message;
	//echo $datetime;
	$sql = 'SELECT * FROM veranstaltungen
		JOIN orte ON orte.idOrte = veranstaltungen.idOrt
		WHERE veranstaltungen.DatumBegin 
		BETWEEN (\''.$datetime.'\' - INTERVAL '.$after.' MINUTE)
		AND (\''.$datetime.'\' + INTERVAL '.$befor.' MINUTE)
		AND orte.Ort = \''.$ort.'\'
		AND NOT batchleser.veranstaltungen.Referent like \'Abgesagt%\''; // filter out Canceled events
	//echo $sql;
	if ($dbconn->Execute($sql) === false) {
			echo "Selection of the event in DB $dbname failed for query:\n";
			print_r($sql);
			exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();
	if (empty($rs)) {
		// try if there is a paralell event
		
		$sql = 'SELECT * FROM veranstaltungen
			JOIN orte ON orte.idOrte = veranstaltungen.idOrt
			WHERE veranstaltungen.DatumBegin 
			BETWEEN (\''.$datetime.'\' - INTERVAL '.$after.' MINUTE)
			AND (\''.$datetime.'\' + INTERVAL '.$befor.' MINUTE)
			AND NOT batchleser.veranstaltungen.Referent like \'Abgesagt%\''; // filter out Canceled events
		//echo $sql;
		if ($dbconn->Execute($sql) === false) {
				echo "Selection of the event in DB $dbname failed for query:\n";
				print_r($sql);
				exit(1);
		} else {
			$rs = $dbconn->Execute($sql);
		}
		$rs = $rs->GetRows();
		if (!empty($rs)){
			$message .= "Event not found in batchleser.veranstaltungen but a parallel event was found";
			return $rs[0];
		}
		echo "Event not found in batchleser.veranstaltungen";
		exit(1);
	}
	return $rs[0];
}

function insertEventIntoRET($event,$idPerson){
	// connect to DB
	global $retDBName;
	$dbname = $retDBName;
	global $retDbHost;
	$dbhost = $retDbHost;
	if (!connectToRetDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit(1);
	}
	global $dbconn;
	$eventid = $event['IdQuelle'];
	$keysret = array(
		'ID',
		'INSTS',
		'ID_PERS',
		'DATUM',
		'TYPE',
		'BESCHREIBUNG',
		'DEL',
		'CREDITS',
		'REFERENT',
		'ID_SCHULUNG');

	$keysret = array_combine($keysret, $keysret);
	$typearr = array(
		'' => 'Paper',
		'' => 'Pres',
		'' => 'Congr',
		'Intern' => '4',
		'Blockkurs' => '4', // FIXME: not yet in RET3
		'' => 'Div');
	
	// reformat date
	$retDatum = DateTime::createFromFormat('Y-m-d H:i:s', $event['DatumBegin']);
	$retDatum = $retDatum->format('d.m.Y');

	$dataarr = array(
		$keysret['INSTS'] 			=> date("YmdHis", time()),
		$keysret['ID_PERS'] 		=> $idPerson,
		$keysret['DATUM'] 			=> $retDatum, // dd.mm.yyyy
		$keysret['TYPE'] 			=> $typearr[$event['Typ']],
		$keysret['BESCHREIBUNG'] 	=> 'Ort: ' . $event['Ort'] . "\nReferent: " . $event['Referent'] . "\nTitel: " . $event['Titel'] . "\nBeschreibung: " . $event['Beschreibung'],
		$keysret['CREDITS']			=> '1', // FIXME: get form event.anaesthesie.ch -> batchleser.veranstaltungen -- not yet implemented there
		$keysret['REFERENT'] 		=> $event['Referent'],
		$keysret['ID_SCHULUNG'] 	=> $event['IdQuelle']);
		
	$sql = "SELECT * FROM datr_portfolio WHERE ID_SCHULUNG = '" . $event['IdQuelle']. "' AND ID_PERS ='" . $idPerson . "'";
	if ($dbconn->Execute($sql) === false) {
		echo "Query for event in DB $dbname failed for query:\n";
		print_r($sql);
		exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();
	//print_r($rs);

	if (empty($rs)) {
		$keylist = '';
		$valuelist = '';
		$count = count($dataarr);
		$i = 1;
		foreach ($dataarr as $key => $value) {
			$key = $dbconn->qstr(trim($key), false);
			$key = substr($key, 1, strlen($key) - 2);
			$keylist .= $key;
			if ($i < $count) {
				$keylist .= ', ';
			}
			$value = $dbconn->qstr(trim($value), false);
			$valuelist .= $value;
			if ($i < $count) {
				$valuelist .= ', ';
			}
			$i++;
		}
		$sql = 'insert into datr_portfolio (' . $keylist . ') values (' . $valuelist . ')';
		//echo $sql;
		if ($dbconn->Execute($sql) === false) {
			echo "Insertion of the event $eventid into DB $dbname for user $idPerson failed for query:\n";
			print_r($sql);
			exit(1);
		}
		//print_r($dbconn);
	} else {
		echo 0;
		echo "Entry of event $eventid already exists in DB $dbname for user $idPerson";
		exit;
	}
}

function insertEventIntoBatchleser($event, $kuerzel){
	// connect to DB
	global $batchleserDBName;
	$dbname = $batchleserDBName;
	global $batchleserDbHost;
	$dbhost = $batchleserDbHost;
	if (!connectToBatchleserDB()) {
		echo "Could not connect to DB $dbname on Host $dbhost";
		exit(1);
	}
	global $dbconn;
	$eventid = $event['IdQuelle'];
	
	$sql = "SELECT * FROM besuche 
		JOIN personen 
		ON besuche.idPerson = personen.idPersonen 
		WHERE idVeranstaltung = " . $event['idVeranstaltungen'] . " 
		AND personen.Kuerzel = '" . $kuerzel . "'";
	if ($dbconn->Execute($sql) === false) {
		echo "Query of event in DB $dbname failed for query:\n";
		print_r($sql);
		exit(1);
	} else {
		$rs = $dbconn->Execute($sql);
	}
	$rs = $rs->GetRows();
	//print_r($rs);

	if (empty($rs)) {
		$sql = 'insert into besuche ( idVeranstaltung, idPerson ) 
				values 
				('. $event['idVeranstaltungen'].', (SELECT idPersonen FROM personen 
				WHERE Kuerzel =\'' . $kuerzel . '\'))';
		//echo $sql;
		if ($dbconn->Execute($sql) === false) {
			echo "Insertion of the event $eventid into DB $dbname for user $kuerzel failed for query:\n";
			print_r($sql);
			exit(1);
		}
		//print_r($dbconn);
	} else {
		echo 0;
		echo "Entry of event $eventid already exists in DB $dbname for user $kuerzel";
		exit;
	}
	
}

// Get the combi number of the user
if (isset($_GET['combi'])) {
	$combi = $_GET['combi'];
} else {
	echo 'Arguments combi is missing';
	exit(1);
}
// figure out the location of the event
if (isset($_GET['batchleser'])){
	$ort = findOrt($_GET['batchleser']);
} elseif (isset($_GET['ort'])) {
	$ort = $_GET['ort'];
} else {
	echo 'Arguments batchleser is missing and no ort was provided as substitute';
	exit(1);
}
// figure out when the event takes place
if (isset($_GET['datetime'])) {
	$datetime = $_GET['datetime'];
} else {
	$datetime = date('Y-m-d H:i:s', time());
}
// import the events form the last day and newer
//importFortbildungenFromAnaesthesieCh();
// find the event in the DB
$event			= findEvent($ort, $datetime);
// search for the user in both DBs
$kuerzelPlanOAA		= getKuerzelFromPlanOAA($combi);
$kuerzelBatchleser	= getKuerzelFromBatchleser($combi);
// insert the visit in to the right DB
if ($kuerzelPlanOAA !== false) {
	$idPerson = getIdPersFromRet($kuerzelPlanOAA);
	insertEventIntoRET($event, $idPerson);
} elseif ($kuerzelBatchleser !== false) {
	insertEventIntoBatchleser($event, $kuerzelBatchleser);
} else {
	echo "Could not find a Kürzel";
	exit(1);
}
echo 0 . $message;
exit;

?>
```