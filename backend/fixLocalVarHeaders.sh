#!/usr/bin/env bash

# https://github.com/OpenAPITools/openapi-generator/issues/10255
SCRIPT_DIR=$(realpath $(dirname "$0"))

SED=`which sed`
which gsed
if [[ "x$?" == "x0" ]]; then
	SED=`which gsed`
fi

#pushd "${SCRIPT_DIR}/api/angular/ch/immeditech/imtrack/rest/service/"
for file in "$1*";
do
    echo 'fixing' + $file
    $SED -i 's/localVarHeaders/headers/g' $file;
done
#popd
