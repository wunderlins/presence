# presence

## building

### Development configuration 

Put your site specific configuration into the file `src/main/resources/application.properties.private`. 
This file will be picked up by the build process and bein put into the war and jar file.

Then building with `mvn package` will give you a war file for debugging/hot-reloading 
during development time.

**NOTE:** when running as `war`, `application.properties` must include an absolute path to 
the database file (for example: `database-file = /tmp/presence.sqlite3`). The folrder **must** exist, 
the file will be created if it is missing.

### build targets

- `mvn clean`: clean build directory
- `mvn test`: unit tests
- `mvn clean verify`: integration tests
- `mvn package -DskipTests=true`: will build war for development / debugging
- `mvn package assembly:single -DskipTests=true`: will build "uber jar" which is a self contained java executable
- `mvn verify -D'test=!Test*' -DfailIfNoTests=false`: run integration tests only (skip unit tests)

**NOTE:**: building a release:

```bash
mvn clean package assembly:single
```
You'll find the generated jar under `target/presence-VERSION-jar-with-dependencies.jar`.

## installation

### war deployement

1. open the `presence.war` file with a zip utility
2. edit `WEB-INF/classes/application.properties`
3. set `database-file` to the location where you want to have the dtabase file
4. deploy war file to tomcat server

The database file should now be created at the configured location. If not, you will have to double check if the folder exists and if the war was deployed properly.

### jar deployment

1. install java 11 or newer
2. copy jar file to destination folder.
3. create `application.properties` in the same location as jar file (see example below)
4. copy sqlite data file into the same folder
5. start service with `java -jar presence-VERSION.jar`

#### example application.properties

```ini
#### Database configuration #######################################################
# This property defines where the sqlite database is stored. 
# in debug environment (deployed war) this location must be absolut and 
# reachable by the apache process
#
# if the program is run as ueber jar, the a relative location is 
# relative to the jar file
database-file = presence.sqlite3

#### standalone server (.jar) deployment options ###################################
# this is the tcp port. use > 1024 if you ar running the process as 
# unprivileged user (as you should)
listen_port = 8890
# 0.0.0.0 == listen on all ips
listen_ip   = 0.0.0.0
# disable /swagger-ui/ by setting this value to false/no/0, anything else is treated as yes
enable_swagger_ui = false

#### LDAP configuration ############################################################
ldap_url    = ldaps://aaaa
# this is a user name or a binddn
ldap_binddn = username
# password for simple bind
ldap_pass   = ssssh
# {UID} will be replaced with the legic UID
ldap_filter = (AIUSBLegicUID={UID}) 
# basedn to make the queries perform
ldap_basedn = ou=VPS,ou=SHADOW,o=AI

#### logging configuration #########################################################
log_folder  = .
# possible levels: info, warn, error, debug
log_level = debug
```

## development setup

building as war for development server

```bash
mvn package
```

For all unit tests to work you need to run an ldap server. For your convenience
this project provides a docker installation with suitable schema and test data. 
More details can be found in [../../container/README.md](../../container/README.md).

once the docker image is running, use the following configuration:

```ini
#### Database configuration #######################################################
# This property defines where the sqlite database is stored. 
# in debug environment (deployed war) this location must be absolut and 
# reachable by the apache process
#
# if the program is run as ueber jar, the a relative location is 
# relative to the jar file
database-file = presence.sqlite3

#### standalone server (.jar) deployment options ###################################
# this is the tcp port. use > 1024 if you ar running the process as 
# unprivileged user (as you should)
listen_port = 8890
# 0.0.0.0 == listen on all ips
listen_ip   = 0.0.0.0
# disable /swagger-ui/ by setting this value to false/no/0, anything else is treated as yes
enable_swagger_ui = false

#### LDAP configuration ############################################################
ldap_url    = ldap://localhost:10389
# this is a user name or a binddn
ldap_binddn = cn=admin,dc=mycorp,dc=com
# password for simple bind
ldap_pass   = s3kre33t
# {UID} will be replaced with the legic UID
ldap_filter = (AIUSBLegicUID={UID}) 
# basedn to make the queries perform
ldap_basedn = ou=people,dc=mycorp,dc=com

#### logging configuration #########################################################
log_folder  = .
# possible levels: info, warn, error, debug
log_level = debug
```
