package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.ConfigApiService;
import net.wunderlin.presence.rest.service.impl.ConfigApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class ConfigApiServiceFactory {
    private static final ConfigApiService service = new ConfigApiServiceImpl();

    public static ConfigApiService getConfigApi() {
        return service;
    }
}
