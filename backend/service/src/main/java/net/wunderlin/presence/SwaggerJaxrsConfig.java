package net.wunderlin.presence;

import org.glassfish.jersey.server.ResourceConfig;

import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import io.swagger.jaxrs.config.BeanConfig;

public class SwaggerJaxrsConfig extends ResourceConfig {

    public SwaggerJaxrsConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("Swagger API Title");
        beanConfig.setVersion("1.0.0");        
        beanConfig.setSchemes(new String[] { "http" });
        beanConfig.setHost("localhost:8090");
        beanConfig.setBasePath("/swagger-ui");
        beanConfig.setResourcePackage("net.wunderlin.presence");
        beanConfig.setScan(true);        
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, Boolean.TRUE);
        packages("net.wunderlin.presence");
        packages("io.swagger.jaxrs.listing");

        register(RolesAllowedDynamicFeature.class);
    }
}