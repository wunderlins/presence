package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.EventApiService;
import net.wunderlin.presence.rest.service.impl.EventApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventApiServiceFactory {
    private static final EventApiService service = new EventApiServiceImpl();

    public static EventApiService getEventApi() {
        return service;
    }
}
