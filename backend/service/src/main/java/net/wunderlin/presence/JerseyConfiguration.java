package net.wunderlin.presence;

import org.glassfish.jersey.server.ResourceConfig;

public class JerseyConfiguration extends ResourceConfig {
    public JerseyConfiguration() {
        packages("net.wunderlin.presence");
        packages("io.swagger.jaxrs.listing");
    }
}
