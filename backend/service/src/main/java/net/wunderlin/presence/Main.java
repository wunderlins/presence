package net.wunderlin.presence;

import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Properties;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

/**
 * Main class.
 *
 */
public class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);
    //protected static PresenceService presenceService = null;
    protected static ConfigService configService = ConfigService.getInstance();
    protected static DaoService daoService = null;

    // Base URI the Grizzly HTTP server will listen on
    public static String BASE_URI = "http://0.0.0.0:8080/api";

    /**
     * Resource config for this service
     *
     * includes:
     * - CORS filter
     * - Swagger config
     * - Jersey configuration
     *
     * @return
     */
    public static ResourceConfig resourceConfig() {
        // create a resource config that scans for JAX-RS resources and providers
        // in net.wunderlin package
        //final ResourceConfig rc = new ResourceConfig().packages("net.wunderlin");
        logger.info("Loading JerseyConfiguration");
        ResourceConfig rc = new JerseyConfiguration();
        //final ResourceConfig rc = new SwaggerJaxrsConfig();

        logger.info("Loading CorsFilter");
        rc.register(CorsFilter.class);

        //logger.info("Loading SwaggerJaxrsConfig");
        //rc.register(SwaggerJaxrsConfig.class);

        rc.register(JacksonJsonProvider.class);

        return rc;
    }

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        logger.info("startServer()");

        // initialize resources
        //presenceService = PresenceService.getInstance();
        configService = ConfigService.getInstance();
        daoService = DaoService.getInstance();

        Properties p = configService.getConfig();
        logger.info(p.toString());
        String listen_port = configService.getConfig().getProperty("listen_port").strip();
        String listen_ip = configService.getConfig().getProperty("listen_ip").strip();
        BASE_URI = BASE_URI.replace("8080", listen_port);
        BASE_URI = BASE_URI.replace("0.0.0.0", listen_ip);
        logger.info("BASE_URI: " + BASE_URI);

        // resource config
        ResourceConfig rc = resourceConfig();

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        HttpServer srv = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);

        // adding static resources

        CLStaticHttpHandler staticHttpHandlerRoot = new CLStaticHttpHandler(Main.class.getClassLoader(), "ui/");
        srv.getServerConfiguration().addHttpHandler(staticHttpHandlerRoot, "/");

        String enable_swagger_ui = configService.getConfig().getProperty("enable_swagger_ui").strip().toLowerCase();

        if (!enable_swagger_ui.equals("no") && !enable_swagger_ui.equals("false") && !enable_swagger_ui.equals("0")) {
            CLStaticHttpHandler staticHttpHandlerSwagger = new CLStaticHttpHandler(Main.class.getClassLoader(), "swagger-ui/");
            srv.getServerConfiguration().addHttpHandler(staticHttpHandlerSwagger, "/swagger-ui");
            logger.info("enabling /swagger-ui/");
        }

        return srv;
    }

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        logger.info("Version: " + Main.class.getPackage().getImplementationVersion());

        // start http server
        final HttpServer server = startServer();
        System.out.println(String.format("Presence service running @ "
                + "%s%nHit Ctrl-C to shut down.", BASE_URI.replace("/api", "/")));

        // capture ctrl+c (SIGINT)
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Shutting down server");
                logger.info("Caught SIGINT, Shutting down server");
                server.shutdownNow();
            }
        });

        // main loop, wait until SIGINT
        while (true) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

