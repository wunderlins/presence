package net.wunderlin.presence;

import net.wunderlin.presence.rest.model.*;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventService {
    static Logger logger = LoggerFactory.getLogger(EventService.class);
    protected static DaoService daoService = DaoService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();

    /**
     * singleton instance
     */
    private static EventService INSTANCE;

    /**
     * singleton
     * @return mock service instance
     * @throws Exception
     */
    public static EventService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new EventService();
        }

        return INSTANCE;
    }

    public List<Event> getAll() throws SQLException {
        List<Event> events = new ArrayList<Event>();

        Connection conn = daoService.getConnection();
        logger.debug("Getting all Events");
        String sql = "SELECT ev.*, " +
            "(SELECT group_concat(room_id, ',') FROM event2room WHERE event_id=ev.id) as rooms " +
            "FROM event ev order by ev.deleted, ev.name DESC";
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        pstmt = conn.prepareStatement(sql);
        rs = pstmt.executeQuery();

        while (rs.next()) {
            List<Integer> room_ids = new ArrayList<Integer>();
            if (rs.getString("rooms") != null) {
                for (String e: rs.getString("rooms").split(",")) {
                    room_ids.add(Integer.valueOf(e));
                }
            }
            Event s;
            s = new Event()
                .id(rs.getInt("id"))
                .name(rs.getString("name"))
                .start(DaoService.timeColonAdd(rs.getString("start_time")))
                .end(DaoService.timeColonAdd(rs.getString("end_time")))
                .dow(rs.getInt("dow"))
                .roomIds((room_ids.size() > 0) ? room_ids : new ArrayList<Integer>())
                .deleted(rs.getBoolean("deleted"));
            events.add(s);
        }

        return events;
    }

    public Event get(Integer id, Boolean exlcudeDeleted) {
        Event ev = new Event();
        ResultSet rs = null;

        Connection conn = daoService.getConnection();
        String sql = "SELECT * FROM event WHERE id=?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        try {
            if (rs.next()) {
                if (exlcudeDeleted && rs.getBoolean("deleted"))
                    return null;

                ev = new Event()
                    .id(rs.getInt("id"))
                    .name(rs.getString("name"))
                    .start(DaoService.timeColonAdd(rs.getString("start_time")))
                    .end(DaoService.timeColonAdd(rs.getString("end_time")))
                    .dow(rs.getInt("dow"))
                    .deleted(rs.getBoolean("deleted"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        // fetch room associations
        List<Integer> room_ids = new ArrayList<Integer>();
        try {
            sql = "SELECT room_id from event2room WHERE event_id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, ev.getId());
            ResultSet rs1  = pstmt.executeQuery();
            while (rs1.next()) {
                room_ids.add(rs1.getInt("room_id"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        if (room_ids.size() > 0)
            ev.setRoomIds(room_ids);

        return ev;
    }

    public boolean updateRoomRelations(Integer ev_id, List<Integer> room_ids) {
        Connection conn = daoService.getConnection();
        PreparedStatement pstmt;
        String sql;

        // clear relations
        try {
            sql = "DELETE FROM event2room WHERE event_id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, ev_id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }

        sql = "INSERT INTO event2room (room_id, event_id) VALUES (?, ?)";
        for(Integer i: room_ids) {
            try {
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, i);
                pstmt.setInt(2, ev_id);
                pstmt.executeUpdate();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                return false;
            }

        }

        return true;
    }

    public Event insert(Event ev) {
        Connection conn = daoService.getConnection();

        String sql = "INSERT INTO event (name, start_time, end_time, dow, deleted) VALUES (?, ?, ?, ?, ?)";

        // make sure start and end time are in a valid range and start time is < end time
        String start_time = DaoService.timeColonRemove(ev.getStart());
        String end_time   = DaoService.timeColonRemove(ev.getEnd());
        int start_h = Integer.parseInt(start_time) / 100;
        int start_m = Integer.parseInt(start_time) % 100;
        int end_h   = Integer.parseInt(end_time) / 100;
        int end_m   = Integer.parseInt(end_time) % 100;
        //logger.info("start {}:{}, end {}:{}", start_h, start_m, end_h, end_m);

        if (start_m > 59) {
            logger.error("start time minute is greater than 59: {}", start_m);
            return null;
        }

        if (end_m > 59) {
            logger.error("end time minute is greater than 59: {}", end_m);
            return null;
        }

        if (start_h > 23) {
            logger.error("start time hour is greater than 23: {}", start_h);
            return null;
        }
        
        if (end_h > 23) {
            logger.error("end time hour is greater than 23: {}", end_h);
            return null;
        }

        if (Integer.parseInt(start_time) >= Integer.parseInt(end_time)) {
            logger.error("end time must be greater than start time: start {} < end {}", start_time, end_time);
            return null;
        }

        // insert record
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, ev.getName());
            pstmt.setString(2, start_time); 
            pstmt.setString(3, end_time);
            pstmt.setInt(4, ev.getDow());
            pstmt.setInt(5, ev.getDeleted() ? 1 : 0);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }


        // get new id
        int last_id = -1;
        try {
            sql = "SELECT MAX(id) as c FROM event";
            Statement stmt_id  = conn.createStatement();
            ResultSet rs_id    = stmt_id.executeQuery(sql);

            // loop through the result set
            while (rs_id.next()) {
                last_id = rs_id.getInt("c");
            }
            ev.setId(last_id);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        // insert room relations
        List<Integer> room_ids = ev.getRoomIds();
        if (ev.getRoomIds() == null)
            room_ids = new ArrayList<Integer>();

        if (!this.updateRoomRelations(ev.getId(), room_ids))
            return null;

        return ev;
    }

    public boolean update(Event ev) {
        Connection conn = daoService.getConnection();
        String sql = "update event set name=?, start_time=?, end_time=?, dow=?, deleted=? where id=?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, ev.getName());
            pstmt.setString(2, DaoService.timeColonRemove(ev.getStart()));
            pstmt.setString(3, DaoService.timeColonRemove(ev.getEnd()));
            pstmt.setInt(4, ev.getDow());
            pstmt.setInt(5, ev.getDeleted() ? 1 : 0);
            pstmt.setInt(6, ev.getId());
            pstmt.executeUpdate();

            // update room relations
            if (ev.getRoomIds() != null) {
                if (!this.updateRoomRelations(ev.getId(), ev.getRoomIds()))
                    return false;
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }


        return true;
    }

    public boolean purge(Event ev) {
        Connection conn = daoService.getConnection();
        PreparedStatement pstmt;
        String sql;

        // clear relations
        try {
            sql = "DELETE FROM event2room WHERE event_id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, ev.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }

        try {
            sql = "DELETE FROM event WHERE id=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, ev.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;

    }

    public boolean delete(Event ev) {
        ev.setDeleted(true);
        boolean ret = this.update(ev);

        return ret;
    }
}
