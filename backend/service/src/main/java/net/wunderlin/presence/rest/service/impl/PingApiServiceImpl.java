package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Pong;

import java.util.Date;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class PingApiServiceImpl extends PingApiService {
    static Logger logger = LoggerFactory.getLogger(PingApiServiceImpl.class);

    @Override
    public Response ping(SecurityContext securityContext) throws NotFoundException {
        // response shall contain hostname and system time:
        String hostname = "";
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            //e.printStackTrace();
            BackendError err = new BackendError()
                .code(1)
                .message(e.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        Pong res = new Pong().hostname(hostname).time(now);
        return Response.ok().entity(res).build();
    }
}
