package net.wunderlin.presence.rest.service;

import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.models.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

public class Bootstrap extends HttpServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    Info info = new Info()
      .title("Presence API")
      .description("Presence API")
      .termsOfService("")
      .contact(new Contact()
        .email("swunderlin@gmail.com"))
      .license(new License()
        .name("")
        .url("http://unlicense.org"));

    //ServletContext context = config.getServletContext();
    Swagger swagger = new Swagger().info(info);

    new SwaggerContextService().withServletConfig(config).updateSwagger(swagger);
  }
}
