package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.EventService;
import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Event;

import java.util.Date;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventApiServiceImpl extends EventApiService {

    static Logger logger = LoggerFactory.getLogger(EventApiServiceImpl.class);
    public EventService es = new EventService();

    @Override
    public Response deleteEvent( @Min(0)Integer id, SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }

    @Override
    public Response getEvent( @Min(0)Integer id, SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }

    @Override
    public Response insertEvent(Event event, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);

        // input validation
        String err_msg = "";
        if (event.getEnd() == "") err_msg += "`end` cannot be empty, ";
        if (event.getStart() == "") err_msg += "`start` cannot be empty, ";
        if (event.getName() == "") err_msg += "`name` cannot be empty, ";
        if (event.getRoomIds().size() == 0) err_msg += "`Rooms` cannot be empty, ";

        if (err_msg != "") {
            BackendError err = new BackendError()
                .code(23)
                .message("Failed to create Event: " + err_msg)
                .ts(now);
                return Response.status(400).entity(err).build();
        }

        Event ret = es.insert(event);
        if (ret == null) {
            logger.error("Failed to create Event");
            BackendError err = new BackendError()
                .code(19)
                .message("Failed to create Event")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.ok().entity(ret).build();
    }

    @Override
    public Response updateEvent( @Min(0)Integer id, Event event, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        event.setId(id); // url parameter always overrides data

        Event e = es.get(id, false);

        // 400, failed to fetch
        if (e == null) {
            logger.error("Failed to update Event, could not fetch Event");
            BackendError err = new BackendError()
                .code(22)
                .message("Failed to update Event, could not fetch Event")
                .ts(now);
                return Response.status(400).entity(err).build();
        }

        // 404, not found
        if (e.getId() == 0) {
            logger.error("Failed to update Event, not found");
            BackendError err = new BackendError()
                .code(21)
                .message("Failed to update Event, not found")
                .ts(now);
                return Response.status(404).entity(err).build();
        }

        boolean ret = es.update(event);
        if (ret == false) {
            logger.error("Failed to update Event");
            BackendError err = new BackendError()
                .code(20)
                .message("Failed to update Event")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.ok().entity(event).build();
    }
}
