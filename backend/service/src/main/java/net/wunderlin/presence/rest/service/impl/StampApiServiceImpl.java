package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.ConfigService;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.EventService;
import net.wunderlin.presence.PresenceService;
import net.wunderlin.presence.RoomService;
import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Room;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.naming.NamingException;
import javax.validation.constraints.NotNull;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class StampApiServiceImpl extends StampApiService {
    static Logger logger = LoggerFactory.getLogger(StampApiServiceImpl.class);
    protected static DaoService daoService = DaoService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();
    protected static ConfigService configService = ConfigService.getInstance();
    protected static RoomService roomService = RoomService.getInstance();
    protected static EventService eventService = EventService.getInstance();

    protected StampRecord lastRecord(String uid) {
        StampRecord res = null;

        return res;
    }


    @Override
    public Response insert(Stamp stamp, SecurityContext securityContext) throws NotFoundException {
        logger.debug("insertingRecord:");
        logger.debug(stamp.toString());

        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        StampRecord res = null;

        // sometimes it happens, that the reader fires two events for one card swipe
        // check against a grace period in seconds. if the same card is seen within
        // this period, we ignore the request and send the last record as result
        Connection conn = daoService.getConnection();
        String sql = "select p.* from presence p WHERE p.id = (SELECT max(id) from presence WHERE uid=?) and stamp > datetime(?) and stamp < datetime(?)";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, stamp.getUid());
            pstmt.setString(2, stamp.getStamp().minusSeconds(5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            pstmt.setString(3, stamp.getStamp().plusSeconds(5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

            /*
            PreparedStatementHelper prepHelper = new PreparedStatementHelper(pstmt);

            try {
                Pattern pattern = Pattern.compile("\\?");
                Matcher matcher = pattern.matcher(sql);
                StringBuffer sb = new StringBuffer();
                int indx = 1;  // Parameter begin with index 1
                while (matcher.find()) {
                    matcher.appendReplacement(sb, prepHelper.getParameter(indx++));
                }
                matcher.appendTail(sb);
                logger.debug("Executing Query [" + sb.toString() + "] with Database[presence] ...");
            } catch (Exception ex) {
                prepHelper.close();
                logger.debug("Executing Query [" + sql + "] with Database[presence] ...");
            }
            prepHelper.close();
            */


            rs = pstmt.executeQuery();
        } catch (SQLException e1) {
            logger.error(e1.getMessage());
            BackendError err = new BackendError()
                .code(6)
                .message(e1.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        try {
            while (rs.next()) {
                /*
                LocalDateTime dt = LocalDateTime.parse(rs.getString("stamp"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                StampRecord s = new StampRecord()
                    .id(rs.getInt("id"))
                    .location(rs.getString("location"))
                    .uid(rs.getString("uid"))
                    .name(rs.getString("person"))
                    .sAMAccountName(rs.getString("sAMAccountName"))
                    .time(OffsetDateTime.of(dt, DaoService.zoneOffSet));

                // check if we have a vey recent stamp, if so, don't save and return old record
                LocalDateTime future  = dt.plusSeconds(5); // we don't accept the same card within 5 seconds
                //if (future.isBefore(LocalDateTime.now()) && dt.isAfter(LocalDateTime.now())) {
                if (future.isBefore(dt)) {
                    logger.info("debouncing double stamp of " + stamp.getUid());
                    return Response.ok().entity(s).build();
                }

                */
                java.util.Date dt = new java.util.Date(rs.getDate("stamp").getTime());
                StampRecord s = new StampRecord()
                    .id(rs.getInt("id"))
                    .location(rs.getString("location"))
                    .uid(rs.getString("uid"))
                    .name(rs.getString("person"))
                    .sAMAccountName(rs.getString("sAMAccountName"))
                    .time(dt.toInstant().atOffset(DaoService.zoneOffSet));
                logger.info(s.toString());
                return Response.ok().entity(s).build();
            }
        } catch (SQLException e1) {
            logger.error(e1.getMessage());
            BackendError err = new BackendError()
                .code(6)
                .message(e1.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        try {
            res = insertStamp(stamp);
        } catch (SQLException e) {
            //System.out.println(e.getMessage());
            logger.error(e.getMessage());
            BackendError err = new BackendError()
                .code(3)
                .message(e.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            logger.error(e.getMessage());
            BackendError err = new BackendError()
                .code(4)
                .message(e.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        res.setTime(res.getTime().withOffsetSameLocal(DaoService.zoneOffSet));
        return Response.ok().entity(res).build();
    }

    @Override
    public Response listJson( @NotNull LocalDate day, SecurityContext securityContext) throws NotFoundException {

        if (day == null) day = LocalDate.now();

        List<StampRecord> stamps = null;

        try {
            stamps = getListOfDay(day);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            BackendError err = new BackendError()
                .code(5)
                .message(e.toString())
                .ts(new Date().toInstant().atOffset(DaoService.zoneOffSet));
                return Response.status(500).entity(err).build();
        }

        return Response.ok().entity(stamps).build();
    }

    @Override
    public Response listCsv( @NotNull LocalDate day, SecurityContext securityContext) throws NotFoundException {

        if (day == null) day = LocalDate.now();

        List<StampRecord> stamps = null;

        try {
            stamps = getListOfDay(day);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            BackendError err = new BackendError()
                .code(5)
                .message(e.toString())
                .ts(new Date().toInstant().atOffset(DaoService.zoneOffSet));
                return Response.status(500).entity(err).build();
        }

        // build CSV from data
        String DELIM_REC   = configService.getConfig().getProperty("csv_row_delimiter", "\r\n");
        String DELIM_FIELD = configService.getConfig().getProperty("csv_field_delimiter", ";");
        // add a bom to the utf8 string so excel recognizes the utf-8 content encoding
        String buffer = '\ufeff' + "Person"+DELIM_FIELD+"Standort"+DELIM_FIELD+"Login Name"+DELIM_FIELD+"Badge ID"+DELIM_FIELD+"Zeit" + DELIM_REC;
        for (StampRecord rec: stamps) {
            String name = rec.getName();
            String time = rec.getTime().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
            String loc  = rec.getLocation();
            String evt  = rec.getEvent();
            String acc  = rec.getsAMAccountName();
            String uid  = rec.getUid();
            buffer += ((name == null) ? "n/a" : name) + DELIM_FIELD +
                      ((loc == null)  ? "n/a" : evt)  + DELIM_FIELD +
                      ((loc == null)  ? "n/a" : loc)  + DELIM_FIELD +
                      ((acc == null)  ? "n/a" : acc)  + DELIM_FIELD +
                      ((uid == null)  ? "n/a" : uid)  + DELIM_FIELD +
                      ((time == null) ? "n/a" : time) + DELIM_REC;
        }

        String filename = "Presence-" + day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ".csv";
        return Response.ok()
            .entity(buffer)
            .header("Content-Disposition", "attachment; filename=\""+filename+"\"")
            .header("Content-Type", "text/csv; charset=utf-8")
            .build();
    }

    // API methods ////////////////////////////////////////////////////////////////////
    public StampRecord insertStamp(Stamp stamp) throws SQLException, NamingException, Exception {
        //OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        Connection conn = daoService.getConnection();

        // resolve UID against ldap
        StampRecord res = presenceService.getResultFromUid(stamp.getUid());
        if (res.getsAMAccountName() == null) {
            // some badges are stored with a diffeenct byte order in the IDM
            // reorder byts, from 0123 -> 0321
            String reordered = "" + stamp.getUid().charAt(0) + stamp.getUid().charAt(1) +
                                    stamp.getUid().charAt(6) + stamp.getUid().charAt(7) +
                                    stamp.getUid().charAt(4) + stamp.getUid().charAt(5) +
                                    stamp.getUid().charAt(2) + stamp.getUid().charAt(3);
            res = presenceService.getResultFromUid(reordered);
        }
        res.setUid(stamp.getUid());
        res.setLocation(stamp.getLocation());
        res.setTime(stamp.getStamp());
        if (res.getName() == null) {
            logger.warn("Failed to resolve UID '{}', no name found.", stamp.getUid());
        }

        // check if we find a room name for this device in the room table
        Room r = roomService.getByDevice(stamp.getLocation());
        if (r != null && r.getDeviceName() != null) {
            res.setLocation(r.getRoomName());
            res.setLocationId(r.getId());
        }

        // check if this stamp fits into an event
        res.setEventId(daoService.findEvent(res));
        if (res.getEventId() != -1)
            res.setEvent(eventService.get(res.getEventId(), false).getName());

        // create database entry
        String sql = "INSERT INTO presence (uid, device, location, location_id, stamp, person, samaccountname, event, event_id) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, stamp.getUid());
        pstmt.setString(2, stamp.getLocation());
        pstmt.setString(3, res.getLocation());
        pstmt.setInt(4, res.getLocationId());
        pstmt.setString(5, stamp.getStamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        pstmt.setString(6, res.getName());
        pstmt.setString(7, res.getsAMAccountName());
        pstmt.setString(8, res.getEvent());
        pstmt.setInt(9, res.getEventId());
        pstmt.executeUpdate();

        // return new id
        int last_id = -1;
        sql = "SELECT MAX(id) as c FROM presence";
            Statement stmt_id  = conn.createStatement();
            ResultSet rs_id    = stmt_id.executeQuery(sql);

        // loop through the result set
        while (rs_id.next()) {
            last_id = rs_id.getInt("c");
        }
        res.setId(last_id);

        return res;
    }

    public List<StampRecord> getListOfDay(LocalDate day) throws SQLException {
        List<StampRecord> stamps = new ArrayList<StampRecord>();
        Connection conn = daoService.getConnection();
        String sqlite_date = day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        logger.debug("Getting listing for date: " + sqlite_date);
        //String sql = "select * from presence where stamp > DATE(?) AND stamp < DATE(?, '+1 day')";
        String sql = "select p.id as id, p.stamp as stamp, p.uid as uid, " +
            "    CASE WHEN r.room IS NOT NULL " +
            "    THEN r.room " +
            "    ELSE p.location " +
            "    END AS location, " +
            "    p.person as person, p.samaccountname as sAMAccountName, event_id, location_id, event " +
            "from presence p left join room r on p.location = r.device " +
            "where p.stamp > DATE(?) AND p.stamp < DATE(?, '+1 day')";
        ResultSet rs = null;

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, sqlite_date);
        pstmt.setString(2, sqlite_date);
        rs = pstmt.executeQuery();

        while (rs.next()) {
            LocalDateTime dt = LocalDateTime.parse(rs.getString("stamp"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            StampRecord s = new StampRecord()
                .id(rs.getInt("id"))
                .location(rs.getString("location"))
                .uid(rs.getString("uid"))
                .name(rs.getString("person"))
                .sAMAccountName(rs.getString("sAMAccountName"))
                .time(OffsetDateTime.of(dt, DaoService.zoneOffSet))
                .locationId(rs.getInt("location_id"))
                .event(rs.getString("event"))
                .eventId(rs.getInt("event_id"));
            stamps.add(s);
        }

        return stamps;
    }
}
