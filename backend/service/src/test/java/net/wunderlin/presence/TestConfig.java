package net.wunderlin.presence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.Room;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;
import net.wunderlin.presence.rest.service.impl.StampApiServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

public class TestConfig {
    static Logger logger = LoggerFactory.getLogger(TestConfig.class);
    protected static ConfigService configService = ConfigService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();
    protected static DaoService daoService = DaoService.getInstance();
    static LocalDateTime dateTime = null;

    /**
     * @throws Exception
     */
    @BeforeAll
    static void beforeAll() throws Exception {
        dateTime = LocalDateTime.parse("2018-05-05T11:50:55");

        // remove test db file
        daoService.close();
        String dbfile = configService.getConfig().getProperty("database-file");
        if (!dbfile.substring(0, 1).equals("/") && // no absolute path and no windows drive letter
            !dbfile.substring(1, 2).equals(":") && // and no unc path
            !dbfile.substring(0, 2).equals("\\\\") ) {
                dbfile = ConfigService.propertiesFolder + "/" + dbfile;
        }

        File dbFileObj = new File(dbfile);
        try {
            logger.info("Removing: " + dbfile);
            dbFileObj.delete();
        } catch (Exception e) {/*do nothing */}
        daoService.open();

        // insert some dummy records
        StampApiServiceImpl svc = new StampApiServiceImpl();
        OffsetDateTime now = dateTime.atOffset(DaoService.zoneOffSet);
        svc.insertStamp(new Stamp().location("loc -2").uid("ADBEEFDE").stamp(now.plusHours(-48)));
        svc.insertStamp(new Stamp().location("loc -1").uid("DEADBEEF").stamp(now.plusHours(-24)));
        svc.insertStamp(new Stamp().location("loc 0").uid("DEADBEEF").stamp(now));
        svc.insertStamp(new Stamp().location("loc 1").uid("ADBEEFDE").stamp(now.plusHours(2)));
        svc.insertStamp(new Stamp().location("loc 2").uid("DEADBEEF").stamp(now.plusHours(-3)));
        svc.insertStamp(new Stamp().location("loc 3").uid("EFDEADBE").stamp(now.plusHours(72)));
        svc.insertStamp(new Stamp().location("loc 4").uid("BEEFDEAD").stamp(now.plusHours(48)));

        // insert imprivata record
        String sql = "INSERT INTO imprivata VALUES('40E490D2','wunderlins','AD634AB-30B-3D4-F8A-AD634AB530B7',"+
                     "NULL,106193,'s.w@abc.com','S. W.')";

        Connection conn = daoService.getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute(sql);
    }

    @AfterEach
    void afterEach() {
        //System.out.println("After each test method");
    }

    @AfterAll
    static void afterAll() {
        //System.out.println("After all test methods");
    }

    /**
     * check if memory database was loaded successfully
     */
    @Test
    void TestInit() {
        // assertNotEquals(0, dbSvc.getDevices().length);
        //System.out.println(PresenceService.propertiesFile);
        logger.info(ConfigService.propertiesFile);
        String dbfile = configService.getConfig().getProperty("database-file");
        assertEquals(true, dbfile.endsWith("stamp.db"));
        String port = configService.getConfig().getProperty("listen_port");
        assertEquals("8890", port);
    }

    @Test
    void TestDbInsert() {
        Connection conn = daoService.getConnection();
        assertNotEquals(null, conn);
        String sql = "INSERT INTO config (key, value) VALUES (?,?)";

        try (
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, "database-file");
            pstmt.setString(2, ConfigService.propertiesFile);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            //System.out.println(e.getMessage());
            logger.error(e.getMessage());
            assertEquals(true, false);
        }
    }

    @Test
    void TestGetListOfDay() throws SQLException {
        LocalDate day = dateTime.toLocalDate();

        StampApiServiceImpl svc = new StampApiServiceImpl();
        List<StampRecord> s = svc.getListOfDay(day);
        //logger.info("number of items: " + s.size());
        assertEquals(3, s.size());

        s = svc.getListOfDay(day.plusDays(1));
        assertEquals(0, s.size());

        s = svc.getListOfDay(day.plusDays(-2));
        assertEquals(1, s.size());
    }

    @Test
    void TestRooms() throws SQLException {
        List<Room> r = new ArrayList<>();
        /*
        r.add(new Room().deviceName("device-1").roomName("Room 1"));
        r.add(new Room().deviceName("device-2").roomName("Room 2"));
        r.add(new Room().deviceName("device-3").roomName("Room 3"));
        r.add(new Room().deviceName("device-4").roomName("Room 4"));
        */

        r.add(new Room().deviceName("presence-reader-01").roomName("Radiologie Demonstrationsraum 1"));
        r.add(new Room().deviceName("presence-reader-02").roomName("Radiologie Demonstrationsraum 2"));
        r.add(new Room().deviceName("presence-reader-03").roomName("Radiologie Demonstrationsraum 3"));
        r.add(new Room().deviceName("presence-reader-04").roomName("Radiologie Konferenzraum 4"));
        r.add(new Room().deviceName("presence-reader-05").roomName("UKBB 2. OG"));
        r.add(new Room().deviceName("presence-reader-06").roomName("Pathologie Bibliothek"));


        RoomService svc =  new RoomService();
        boolean ret = svc.saveAll(r);
        logger.info("Insert test: " + ret);
        assertEquals(true, ret);

        List<Room> rooms = svc.getAll();
        assertEquals(6, rooms.size());
    }

    @Test
    void TestLocalLegicId() throws NamingException, Exception {
        StampRecord r = presenceService.getResultFromUid("D290E440");

        assertEquals("S. W.", r.getName());
    }

}
