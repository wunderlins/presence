package net.wunderlin.presence;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.Event;
import net.wunderlin.presence.rest.model.Pong;
import net.wunderlin.presence.rest.model.Room;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ITestRestApi {
    static LocalDateTime dateTime = LocalDateTime.parse("2018-05-15T11:50:55");
    static LocalDateTime dateTimeUnitTest = LocalDateTime.parse("2018-05-05T11:50:55");
    static RoomService roomApi = new RoomService();

    static HttpServer server = null;
    static Logger logger = LoggerFactory.getLogger(ITestRestApi.class);
    protected static PresenceService presenceService = PresenceService.getInstance();
    protected static DaoService daoService = DaoService.getInstance();
    protected static WebTarget target = null;

    @BeforeAll
    static void beforeAll() throws Exception {
        logger.info("beforeAll()");
        server = Main.startServer();

        // setup test framework
        Client c = ClientBuilder.newClient().register(JacksonConfig.class);

        target = c.target(Main.BASE_URI);
        target.register(ITestRestApi.class);

        //Json.mapper().registerModule(new Jdk8Module());
        //Json.mapper().registerModule(new JavaTimeModule());

        // remove rooms by name
        //List<Room> newRrooms = new ArrayList<Room>();
        List<Room> rooms = ITestRestApi.roomApi.getAll();
        for(Room ir: rooms) {
            if (ir.getRoomName() == "Room 5" ||
                ir.getRoomName() == "Room 6" ||
                ir.getRoomName() == "Room 7" ||
                ir.getRoomName() == "Room 8" ||
                ir.getRoomName() == "Room 9" ||
                ir.getRoomName() == "Room 10"
            ) {
                ITestRestApi.roomApi.delete(ir);
            }
        }
    }

    @AfterAll
    static void afterAll() {
        System.out.println("afterAll()");
        server.shutdownNow();
    }

    @AfterEach
    void afterEach() {
        //System.out.println("After each test method");
    }

    @Test
    void TestInit() {
        logger.info("TestInit()");
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     * @throws UnknownHostException
     */
    @Test
    public void TestPing() throws UnknownHostException {
        Response response = target.path("ping").request().get();
        //logger.info(response.readEntity(String.class));
        Pong pong = response.readEntity(Pong.class);
        assertEquals(pong.getHostname(), InetAddress.getLocalHost().getHostName());
    }

    @Test
    public void TestCreateStamp() throws JsonMappingException, JsonProcessingException {
        //OffsetDateTime dateTimeOffset = dateTime.atOffset(DaoService.zoneOffSet);
        //Json.mapper().registerModule(new JavaTimeModule());
        //Json.mapper().registerModule(new JSR310Module());

        OffsetDateTime dateTimeOffset = OffsetDateTime.now();
        Stamp stamp = new Stamp()
            .location("Test Location")
            .stamp(dateTimeOffset)
            .uid("DEADBEEF");

        logger.info(Entity.entity(stamp, MediaType.APPLICATION_JSON).toString());
        Response response = target.path("stamp")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(stamp, MediaType.APPLICATION_JSON));

        //String res = response.readEntity(String.class);
        //logger.info(res);
        StampRecord record = response.readEntity(StampRecord.class);
        //StampRecord record = om.readValue(res, StampRecord.class);
        assertEquals(200, response.getStatus());
        assertEquals("Test Location", record.getLocation());
        assertEquals("DEADBEEF", record.getUid());
        assertEquals("Person One", record.getName());
        assertEquals("persono", record.getsAMAccountName());
        assertEquals(true, (record.getId() > 0));

        // debounce test, no new id
        Response response2 = target.path("stamp")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(stamp, MediaType.APPLICATION_JSON));
        //String res = response2.readEntity(String.class);
        //logger.debug(res);
        StampRecord record2 = response2.readEntity(StampRecord.class);
        assertEquals(record.getId(), record2.getId());

        // shift 10 secs and we should get a new id
        stamp.setStamp(stamp.getStamp().plusSeconds(10));
        Response response3 = target.path("stamp")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(stamp, MediaType.APPLICATION_JSON));
        StampRecord record3 = response3.readEntity(StampRecord.class);
        assertEquals(record2.getId()+1, record3.getId());
    }

    @Test
    public void TestGetList() {
        // get a list of stamps created by the unit tests method beforeAll()
        String day = dateTimeUnitTest.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Response response = target.path("stamp").queryParam("day", day).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        //logger.info(response.getHeaders().toString());

        StampRecord[] recs = response.readEntity(StampRecord[].class);
        //logger.info(recs.toString());
        assertEquals(3, recs.length);
        assertEquals("persono", recs[0].getsAMAccountName());
        assertEquals("Person One", recs[0].getName());
        assertEquals("DEADBEEF", recs[0].getUid());
        assertEquals("loc 0", recs[0].getLocation());

        assertEquals("persont", recs[1].getsAMAccountName());
        assertEquals("Person Two", recs[1].getName());
        assertEquals("ADBEEFDE", recs[1].getUid());
        assertEquals("loc 1", recs[1].getLocation());

        assertEquals("persono", recs[2].getsAMAccountName());
        assertEquals("Person One", recs[2].getName());
        assertEquals("DEADBEEF", recs[2].getUid());
        assertEquals("loc 2", recs[2].getLocation());

    }

    @Test
    public void TestRooms() {
        List<Room> r = new ArrayList<>();
        r.add(new Room().deviceName("device-5").roomName("Room 5"));
        r.add(new Room().deviceName("device-6").roomName("Room 6"));
        r.add(new Room().deviceName("device-7").roomName("Room 7"));
        r.add(new Room().deviceName("device-8").roomName("Room 8"));

        // test write
        /*Response response_post = */ target.path("rooms")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(r, MediaType.APPLICATION_JSON));


        // test read
        Response response = target.path("rooms").request().get();
        logger.info(response.toString());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Room[] recs = response.readEntity(Room[].class);
        assertTrue((recs.length > 4));
    }

    @Test
    public void TestInsertRoom() {
        Room r = new Room().deviceName("device-9").roomName("Room 9");

        // test write
        Response response_post = target.path("room")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(r, MediaType.APPLICATION_JSON));

        assertEquals(Response.Status.OK.getStatusCode(), response_post.getStatus());
        Room rec = response_post.readEntity(Room.class);
        assertNotEquals(null, rec.getId());

        // get room
        Response response_get = target.path("room/" + rec.getId().toString())
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .get();
        assertEquals(200, response_get.getStatus());
    }

    @Test
    public void TestDeleteRoom() {
        Room r = new Room().deviceName("device-10").roomName("Room 10");

        // test write
        Response response_post = target.path("room")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(r, MediaType.APPLICATION_JSON));

        assertEquals(Response.Status.OK.getStatusCode(), response_post.getStatus());
        Room rec = response_post.readEntity(Room.class);
        assertNotEquals(null, rec.getId());

        // delete room again
        Response response_delete = target.path("room/" + rec.getId().toString())
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .delete();
        assertEquals(Response.Status.OK.getStatusCode(), response_delete.getStatus());

        Response response_get = target.path("room/" + rec.getId().toString())
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .get();
        assertEquals(404, response_get.getStatus());

    }

    @Test
    public void TestUpdateRoom() {
        Room r = new Room().deviceName("device-12").roomName("Room 12");

        // test write
        Response response_post = target.path("room")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(r, MediaType.APPLICATION_JSON));

        assertEquals(Response.Status.OK.getStatusCode(), response_post.getStatus());
        Room rec = response_post.readEntity(Room.class);
        assertNotEquals(null, rec.getId());

        // now set a new name
        rec.setRoomName("Room 12.1");
        Response response_put = target.path("room/" + rec.getId().toString())
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .put(Entity.entity(rec, MediaType.APPLICATION_JSON));
        assertEquals(Response.Status.OK.getStatusCode(), response_post.getStatus());
        Room rec1 = response_put.readEntity(Room.class);

        assertEquals(rec.getRoomName(), rec1.getRoomName());
    }

    @Test
    public void TestInsertEvent() {
        // create a room to be associated with the new event
        Room r1 = new Room()
                    .deviceName("device-evtest-01")
                    .roomName("Room Event Test 1");
        // test write
        Response response_post = target.path("room")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(r1, MediaType.APPLICATION_JSON));
        assertEquals(Response.Status.OK.getStatusCode(), response_post.getStatus());

        // create an event
        Event e1 = new Event()
            .name("ITest Event 01")
            .roomIds(Arrays.asList(r1.getId()))
            .start("12:00")
            .end("13:00")
            .dow(2) // wednesday
            ;
    }
}
