package net.wunderlin.presence;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.StampRecord;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


public class TestLdap {
    static boolean disabled = false;
    static Logger logger = LoggerFactory.getLogger(TestLdap.class);
    protected static ConfigService configService = ConfigService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();

    public static void printSearchEnumeration(NamingEnumeration<SearchResult> retEnum) {
        try {
            while (retEnum.hasMore()) {
                SearchResult sr = (SearchResult) retEnum.next();
                logger.info(">>" + sr.getNameInNamespace());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TestConnect() {
        if (disabled) return;
        /*
        LdapUrl url = new LdapUrl("ldaps://ictidmldplp01.uhbs.ch:636");
        LdapConnectionConfig sslConfig = new LdapConnectionConfig();
        sslConfig.setLdapHost(url.getHost());
        sslConfig.setUseSsl(true);
        sslConfig.setLdapPort(url.getPort());
        sslConfig.setTrustManagers(new NoVerificationTrustManager());
        sslConfig.setSslProtocol("TLS10");
        try (
            LdapConnection connection = new LdapNetworkConnection(sslConfig)
        )
        {
            connection.bind("cn=ldapread,o=services", "idmreadldap");
            assertEquals(true, ((LdapNetworkConnection)connection).getConfig().isUseSsl());
            assertEquals(true, connection.isAuthenticated());
        }
        */

        // disable cert validation
        //System.setProperty("com.sun.jndi.ldap.object.disableEndpointIdentification", "true");

        /*
        // Set up the environment for creating the initial context
        Hashtable<String, Object> env = new Hashtable<String, Object>(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        String ldap_url    = presenceService.getConfig().getProperty("ldap_url");
        String ldap_binddn = presenceService.getConfig().getProperty("ldap_binddn");
        String ldap_pass   = presenceService.getConfig().getProperty("ldap_pass");
        String ldap_basedn = presenceService.getConfig().getProperty("ldap_basedn");
        String ldap_filter = presenceService.getConfig().getProperty("ldap_filter");
        logger.info("ldap_url:    " + ldap_url);
        logger.info("ldap_binddn: " + ldap_binddn);
        logger.info("ldap_pass:   " + ldap_pass);
        logger.info("ldap_basedn: " + ldap_basedn);
        logger.info("ldap_filter: " + ldap_filter);

        env.put(Context.PROVIDER_URL, ldap_url);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, ldap_binddn);
        env.put(Context.SECURITY_CREDENTIALS, ldap_pass);

        // Perform search in the entire subtree.
        SearchControls ctl = new SearchControls();
        ctl.setSearchScope(SearchControls.SUBTREE_SCOPE);

        try {
            // Create initial context
            DirContext ctx = new InitialDirContext(env);

            String searchFilter = ldap_filter.replace("{UID}", "796C43DA");
            NamingEnumeration<SearchResult> answer = ctx.search(ldap_basedn, searchFilter, ctl);

            // Print the answer
            printSearchEnumeration(answer);

            // Close the context when we're done
            ctx.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        */

        DirContext ctx = presenceService.ldapConnection();
        assertNotEquals(null, ctx);

        if (ctx != null) {
            // search parameters
            String ldap_basedn = configService.getConfig().getProperty("ldap_basedn");
            String ldap_filter = configService.getConfig().getProperty("ldap_filter");
            String searchFilter = ldap_filter.replace("{UID}", "DEADBEEF");

            // Perform search in the entire subtree.
            SearchControls ctl = new SearchControls();
            ctl.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // search entities
            NamingEnumeration<SearchResult> answer = null;
            try {
                answer = ctx.search(ldap_basedn, searchFilter, ctl);
            } catch (NamingException e) {
                logger.error("LDAP search failed, reason: " + e.getMessage());
                try {
                    ctx.close();
                } catch (NamingException e1) {
                    // ignore, we try to be nice and close connections whenever possible.
                    logger.warn("Failed to close LDAP connection, reason: " + e.getMessage());
                }
            }
            assertNotEquals(null, answer);

            // we are done, close connection
            try {
                if (answer != null)
                    ctx.close();
            } catch (NamingException e) {
                logger.error("Failed to clsoe LDAP connection, reason: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Test
    void TestLookupUID() throws NamingException, Exception {
        if (disabled) return;

        // find peter althaus
        StampRecord res = presenceService.getResultFromUid("ADBEEFDE");
        assertEquals("Person Two", res.getName());

        // search for user name with an invald badge uid
        res = presenceService.getResultFromUid("aaaaaxxxx");
        assertEquals(null, res.getName());
    }
}
