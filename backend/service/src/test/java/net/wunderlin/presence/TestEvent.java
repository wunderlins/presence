package net.wunderlin.presence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.Event;
import net.wunderlin.presence.rest.model.Room;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestEvent {
    static Logger logger = LoggerFactory.getLogger(TestEvent.class);
    protected static PresenceService presenceService = PresenceService.getInstance();
    protected static DaoService daoService = DaoService.getInstance();
    public EventService eventApi = new EventService();
    public RoomService roomApi = new RoomService();
    static LocalDateTime dateTime = null;

    /**
     * @throws Exception
     */
    @BeforeAll
    static void beforeAll() throws Exception {
        dateTime = LocalDateTime.parse("2018-05-05T11:50:55");
    }

    @AfterEach
    void afterEach() {
        //System.out.println("After each test method");
    }

    @AfterAll
    static void afterAll() {
        //System.out.println("After all test methods");
    }

    /**
     * check if memory database was loaded successfully
     * @throws SQLException
     */
    @Test
    void TestEvent0() throws SQLException {
        logger.info("Testing Insert Event API");
        // create a new Room
        Event r1 = new Event().name("Ev 1").start("08:00").end("09:00").dow(1);
        Event ret = this.eventApi.insert(r1);
        assertNotEquals(null, ret); // returns null on error
        assertNotEquals(0, ret.getId()); // default id is 0

        this.eventApi.insert(new Event().name("Ev 2").start("12:00").end("13:00").dow(2));
        this.eventApi.insert(new Event().name("Ev 3").start("12:30").end("14:30").dow(6));

        // get all rooms and find new room
        List<Event> events = this.eventApi.getAll();
        Event r = null;
        for(Event ev: events) {
            if (ev.getId() == ret.getId()) {
                r = ev;
                break;
            }
        }
        assertEquals(ret, r);
        assertNotNull(r);
        assertEquals("Ev 1", r.getName());
        assertEquals("08:00", r.getStart());
        assertEquals("09:00", r.getEnd());
        assertEquals(1, r.getDow());

			/*

      // update room
      r1.setDeviceName("D1.1");
      boolean update_ret = this.roomApi.updateRoomApi(r1);
      assertEquals(true, update_ret);

      // fetch again and compare name
      Room updated = this.roomApi.getRoomApi(ret.getId());
      assertEquals(r1.getDeviceName(), updated.getDeviceName());

      // delete Room again
      boolean delRoom = this.roomApi.deleteRoomApi(r);
      assertEquals(true, delRoom);

      // get all rooms and find new room
      rooms = this.roomApi.getRoomsApi();
      r = null;
      for(Room ir: rooms) {
        if (ir.getId() == ret.getId()) {
          r = ir;
          break;
        }
      }
      assertEquals(null, r);

      // get room
      r = this.roomApi.getRoomApi(ret.getId());
      assertEquals(0, r.getId());
			*/
    }

    @Test
    void TestEventDelete() throws SQLException {
        logger.info("Testing Delete Event API");
        Event r1 = new Event().name("Ev XXX").start("08:00").end("09:00");
        Event ret = this.eventApi.insert(r1);
        Integer ret_id = ret.getId();

        // get all rooms and find new room
        List<Event> events = this.eventApi.getAll();
        //Integer count = events.size();
        Event r = null;
        for(Event ev: events) {
            if (ev.getId() == ret.getId()) {
                r = ev;
                break;
            }
        }

        // delete it again
        boolean del_ret = this.eventApi.delete(r);
        assertTrue(del_ret);

        Event ev = this.eventApi.get(ret_id, false);
        //Integer new_count = this.eventApi.getAll().size();
        assertEquals(true, ev.getDeleted());
    }


    @Test
    void TestEventUpdate() throws SQLException {
        // create dummy value
        logger.info("Testing Update Event API");
        Event r1 = new Event().name("Ev Update").start("11:00").end("12:00").dow(3);
        Event ret = this.eventApi.insert(r1);

        // update record
        ret.setName("Ev Update updated");
        boolean upd_ret = this.eventApi.update(ret);
        assertEquals(true, upd_ret);

        // fetch again and compare
        Event retg = this.eventApi.get(ret.getId(), false);
        assertEquals(ret, retg);

    }

    @Test
    void TestEventRoomRelations() throws SQLException {
        logger.info("Testing Event<->Room relations");

        Event e1 = new Event().name("Ev Update").start("11:00").end("12:00").dow(4);
        // add all rooms
        List<Room> rooms = roomApi.getAll();
        List<Integer> rlist = new ArrayList<Integer>();
        for (Room r: rooms) {
            rlist.add(r.getId());
        }
        e1.setRoomIds(rlist);
        Event ret = this.eventApi.insert(e1);
        assertEquals(ret.getId(), e1.getId());

        Event retg = this.eventApi.get(ret.getId(), false);
        assertNotNull(retg.getRoomIds());
        assertEquals(rlist, retg.getRoomIds());

        // remove the last room form relation
        List<Integer> rlist2 = rlist.subList(0, rlist.size()-1);
        ret.setRoomIds(rlist2);
        Boolean update_ret = this.eventApi.update(ret);
        assertTrue(update_ret);
        retg = this.eventApi.get(ret.getId(), false);
        assertNotNull(retg.getRoomIds());
        assertEquals(rlist2, retg.getRoomIds());
        assertEquals((rlist.size() - 1), retg.getRoomIds().size());

        // get list, find by id, compare relations
        List<Event> all = this.eventApi.getAll();
        Event evall = null;
        for (Event e: all) {
            if (e.getId() == ret.getId()) {
                evall = e;
                break;
            }
        }
        assertNotNull(evall);
        assertEquals(rlist2, evall.getRoomIds());

        // remove event, make sure the relations are gone too
        Boolean ret_del = this.eventApi.delete(ret);
        assertTrue(ret_del);

        Event retd = this.eventApi.get(ret.getId(), false);
        assertEquals(true, retd.getDeleted());
    }

    @Test
    void TestEventDeleteRoom() throws SQLException {
        logger.info("Testing Event<->Room relations");

        Room rdel = new Room().roomName("Room to delete").deviceName("device to delete");
        rdel = this.roomApi.insert(rdel);
        assertNotNull(rdel); // check if room was created
        Integer room_id = rdel.getId();
        assertNotEquals(0, room_id);

        // new event and add all rooms
        Event e1 = new Event().name("Ev DeleteRoom").start("11:00").end("12:00").dow(3);
        List<Room> rooms = roomApi.getAll();
        List<Integer> rlist = new ArrayList<Integer>();
        for (Room r: rooms) {
            rlist.add(r.getId());
        }
        e1.setRoomIds(rlist);
        Event ret = this.eventApi.insert(e1);
        assertEquals(ret.getId(), e1.getId());

        // delete room -> fail because used in event
        Boolean ret_del = false;
        try {
            ret_del = this.roomApi.delete(rdel);
        } catch (Exception e) {;}
        assertFalse(ret_del);

        // remove room from all events
        this.eventApi.updateRoomRelations(e1.getId(), new ArrayList<Integer>());

        // remove event from room
        this.eventApi.delete(e1);

        /*
        // delete room -> success, no event uses room
        try {
            ret_del = this.roomApi.delete(rdel);
        } catch (Exception e) {;}
        assertTrue(ret_del);

        // get event again and check if room is gone
        ret = this.eventApi.get(ret.getId(), true);

        // room must not be contained in room id list
        assertEquals(false, ret.getRoomIds().contains(room_id));
        */
   }


}
