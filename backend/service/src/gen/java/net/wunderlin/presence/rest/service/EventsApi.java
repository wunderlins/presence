package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.EventsApiService;
import net.wunderlin.presence.rest.service.factories.EventsApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Event;

import java.util.Map;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/events")


@io.swagger.annotations.Api(description = "the events API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventsApi  {
   private final EventsApiService delegate;

   public EventsApi(@Context ServletConfig servletContext) {
      EventsApiService delegate = null;

      if (servletContext != null) {
         String implClass = servletContext.getInitParameter("EventsApi.implementation");
         if (implClass != null && !"".equals(implClass.trim())) {
            try {
               delegate = (EventsApiService) Class.forName(implClass).newInstance();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
         }
      }

      if (delegate == null) {
         delegate = EventsApiServiceFactory.getEventsApi();
      }

      this.delegate = delegate;
   }

    @javax.ws.rs.GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "List of all Events", response = Event.class, responseContainer = "List", tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "list of Events", response = Event.class, responseContainer = "List"),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response listEvents(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.listEvents(securityContext);
    }
}
