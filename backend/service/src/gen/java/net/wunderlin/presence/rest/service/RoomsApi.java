package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.RoomsApiService;
import net.wunderlin.presence.rest.service.factories.RoomsApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import net.wunderlin.presence.rest.model.BackendError;
import java.util.List;
import net.wunderlin.presence.rest.model.Room;

import java.util.Map;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/rooms")


@io.swagger.annotations.Api(description = "the rooms API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomsApi  {
   private final RoomsApiService delegate;

   public RoomsApi(@Context ServletConfig servletContext) {
      RoomsApiService delegate = null;

      if (servletContext != null) {
         String implClass = servletContext.getInitParameter("RoomsApi.implementation");
         if (implClass != null && !"".equals(implClass.trim())) {
            try {
               delegate = (RoomsApiService) Class.forName(implClass).newInstance();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
         }
      }

      if (delegate == null) {
         delegate = RoomsApiServiceFactory.getRoomsApi();
      }

      this.delegate = delegate;
   }

    @javax.ws.rs.POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "mapping from devices to rooms", notes = "Updates the mapping of devices to rooms. Updating requires to send the whole list. This will always replace the existing list. ", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "items updated", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response insertRooms(@ApiParam(value = "") @Valid  List<Room> room,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.insertRooms(room, securityContext);
    }
    @javax.ws.rs.GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "List of rooms to device mappings", response = Room.class, responseContainer = "List", tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "list of room <-> device mappings", response = Room.class, responseContainer = "List"),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response listRooms(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.listRooms(securityContext);
    }
}
