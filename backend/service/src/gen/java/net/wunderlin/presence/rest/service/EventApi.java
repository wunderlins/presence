package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.EventApiService;
import net.wunderlin.presence.rest.service.factories.EventApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Event;

import java.util.Map;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/event")


@io.swagger.annotations.Api(description = "the event API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventApi  {
   private final EventApiService delegate;

   public EventApi(@Context ServletConfig servletContext) {
      EventApiService delegate = null;

      if (servletContext != null) {
         String implClass = servletContext.getInitParameter("EventApi.implementation");
         if (implClass != null && !"".equals(implClass.trim())) {
            try {
               delegate = (EventApiService) Class.forName(implClass).newInstance();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
         }
      }

      if (delegate == null) {
         delegate = EventApiServiceFactory.getEventApi();
      }

      this.delegate = delegate;
   }

    @javax.ws.rs.DELETE
    @Path("/{id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Delete Event", notes = "This method marks an event as delete. the attribute `deleted` will be set to true. ", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "Event deleted", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response deleteEvent(@ApiParam(value = "Numeric ID of the Event to delete", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.deleteEvent(id, securityContext);
    }
    @javax.ws.rs.GET
    @Path("/{id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Get event", notes = "", response = Event.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "item get", response = Event.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response getEvent(@ApiParam(value = "Numeric ID of the event to get", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.getEvent(id, securityContext);
    }
    @javax.ws.rs.POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "create new Event", notes = "Create a new Event ", response = Event.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "items updated", response = Event.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response insertEvent(@ApiParam(value = "") @Valid  Event event,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.insertEvent(event, securityContext);
    }
    @javax.ws.rs.PUT
    @Path("/{id}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Update Event", notes = "", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "item updated", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response updateEvent(@ApiParam(value = "Numeric ID of the Event to update", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@ApiParam(value = "") @Valid  Event event,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.updateEvent(id, event, securityContext);
    }
}
