/**
 * Presence API
 * Presence API
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: swunderlin@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Pong { 
    hostname?: string;
    time?: string;
}

