import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { formatDate } from '@angular/common';
import { SharedModule } from '../shared/shared.module'

@Component({
  selector: 'app-stamp',
  templateUrl: './stamp.component.html',
  styleUrls: ['./stamp.component.css']
})
export class StampComponent implements OnInit {
  displayedColumns: string[] = ['name', 'time', 'event', 'location', 'sAMAccountName', 'uid' ];
  text: String  = 'text';

  constructor(public shared: SharedModule ) { }

  ngOnInit(): void {
    this.shared.getStamps(null);

    this.shared.getConfig();

    // subscribe to config observable
    this.shared.config$.subscribe(evt => {
      this.text = evt.application_info   ?? "";
    });
  }

  dateChange(type: string, event: MatDatepickerInputEvent<Date>) {
    //console.log(event.value)
    var dt: Date = new Date();
    if (event.value != null)
      dt = new Date(event.value);
    let dateString = formatDate(dt ,'yyyy-MM-dd', "en");
    this.shared.getStamps(dateString);
  }

}
