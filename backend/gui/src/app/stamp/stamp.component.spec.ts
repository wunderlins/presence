import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import {
  ApiModule,
  Configuration,
  ConfigurationParameters,
  Stamp,
} from '../api/1.0.0'; 

import { environment } from 'src/environments/environment';
export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
      basePath: environment.backend_url,
  };
  return new Configuration(params);
}
import { SharedModule } from '../shared/shared.module';

import { StampComponent } from './stamp.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('StampComponent', () => {
  let component: StampComponent;
  let fixture: ComponentFixture<StampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [ HttpClientModule, RouterTestingModule, SharedModule ],
      declarations: [ StampComponent ]
    })
    //.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
