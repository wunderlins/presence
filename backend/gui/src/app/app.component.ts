import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  env = environment;

  title: String = 'gui';
  showTestButtons: String = "display: none;"

  constructor(public shared: SharedModule ) { }

  downloadCsv() {
    var dt: Date = new Date();
    if (this.shared.selectedDate.value != null)
      dt = new Date(this.shared.selectedDate.value);
    let dateString = formatDate(dt ,'yyyy-MM-dd', "en");
    let url = this.env.backend_url + "/stamp/csv?day=" + dateString
    document.location.href = url;
  }

  ngOnInit() {
    this.shared.getConfig();

    // subscribe to config observable
    this.shared.config$.subscribe(evt => {
      //console.log(evt)
      this.title = evt.application_title ?? "";
    });

    if (this.env.production === false) {
        this.showTestButtons = 'display: block;';
    }
  }

  testHttpError() {
    this.shared.service.getRoom(999999).subscribe(res => {
    });
  }

}
