import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Room } from '../api/1.0.0/model/room';
import {ErrorStateMatcher} from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class EmptyArrayErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-room-list',
  template: `
    <mat-form-field [ngClass]="class" appearance="fill" [style]="style">
      <mat-label>{{label}}</mat-label>

      <mat-autocomplete #roomList="matAutocomplete" (optionSelected)="selectedOpt($event)">
        <mat-option *ngFor="let option of this.roomListOptions" [value]="option?.id">
          {{option?.room_name}}
        </mat-option>
      </mat-autocomplete>
      <mat-chip-list #chipList aria-label="Room selection">
        <mat-chip *ngFor="let rid of filterEmtpy(selectedRooms)" (removed)="removeOpt(rid)">
          {{options[rid].room_name}}
          <button matChipRemove>
            <mat-icon>cancel</mat-icon>
          </button>
        </mat-chip>
        <input type="text"
              placeholder=""
              aria-label="Rooms"
              matInput
              [errorStateMatcher]="emptyMatcher"
              [matChipInputFor]="chipList"
              [matAutocomplete]="roomList"
              [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
              (matChipInputTokenEnd)="addOpt($event)">
      </mat-chip-list>
    </mat-form-field>
    <!-- {{selectedRooms | json}} -->
  `,
  styles: [

  ]
})
export class RoomListComponent implements OnInit {

  @Input() options: Room[] = [];
  @Input() selectedRooms: number[] = [];
  @Output() selectedRoomsChange = new EventEmitter();
  @Input() label: String = '';
  @Input() class: String = '';
  @Input() style: String = '';

  @Output()
  change: EventEmitter<number[]> = new EventEmitter<number[]>();

  roomListOptions: Room[] = new Array<Room>();

  emptyMatcher = new EmptyArrayErrorStateMatcher();
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor() {
  }

  valueChange() {
    this.selectedRoomsChange.emit(this.selectedRooms);
    this.change.emit(this.selectedRooms);
    this.filterRoomsBySelected();
  }

  ngOnInit(): void {
    //console.log(this.options)
    this.filterRoomsBySelected();
  }

  filterRoomsBySelected() {
    var ret: Room[] = [];
    for(let r of this.options)
        if (r && r.id && this.selectedRooms.indexOf(r.id) == -1)
            ret.push(r);
    this.roomListOptions = ret;
  }

  filterEmtpy(list: any[]): any[] {
    //console.log(list)
    return list.filter(l => l != null);
  }

  private findRoomByName(name: String): Room | null {
    var ret = null;

    for(let r of this.options)
      if (r && r.room_name == name)
        return r;

    return ret;
  }

  selectedOpt(event: MatAutocompleteSelectedEvent) {
    const selectedValue: number = event.option.value;
    console.log(selectedValue)
    if (this.selectedRooms.indexOf(selectedValue) == -1)
      this.selectedRooms.push(selectedValue);
    this.valueChange();
  }

  addOpt(event: MatChipInputEvent): void {

    const input = event.chipInput.inputElement;
    const value = event.value;

    const r: Room | null = this.findRoomByName(value);
    if (r != null && r.id) {
      this.selectedRooms.push(r.id);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.valueChange();
  }

  removeOpt(roomId: number): void {
    const optIndex = this.selectedRooms.indexOf(roomId);
    if (optIndex >= 0) {
      this.selectedRooms.splice(optIndex, 1);
    }
    this.valueChange();
  }

}
