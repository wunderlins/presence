import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { formatDate } from '@angular/common';
import { SharedModule, ToastComponent } from '../shared/'
import { Room } from '../api/1.0.0/model/room'


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  constructor(public shared: SharedModule,
              private toast: ToastComponent ) { }

  ngOnInit(): void {
    this.shared.getRooms();
  }

  saveRooms() {
    // create an array of room objects
    let count = this.shared.getRoomCount();
    var rooms: Room[] = [];
    for (var i=0; i<count; i++) {
      let id = document.getElementById("id_" + i) as HTMLInputElement;
      let d  = document.getElementById("device_" + i) as HTMLInputElement;
      let r  = document.getElementById("room_" + i)   as HTMLInputElement;
      if (d && d.value && r && r.value) {
        rooms.push({
          id: (id.value) ? parseInt(id.value) : 0,
          device_name: (d.value) ? d.value : "",
          room_name:   (r.value) ? r.value : ""
        })
      }
    }

    // new line
    let d = document.getElementById("device_N") as HTMLInputElement;
    let r = document.getElementById("room_N")   as HTMLInputElement;
    if (d && d.value && r && r.value) {
      rooms.push({
        id: 0,
        device_name: (d.value) ? d.value : "",
        room_name:   (r.value) ? r.value : ""
      });
      r.value = "";
      d.value = "";
    }

    console.log(rooms);
    this.shared.updateRooms(rooms);

  }

  deleteRoom(id: number) {
    this.shared.deleteRoom(id);
  }

}
