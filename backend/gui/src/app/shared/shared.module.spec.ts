import { ComponentFixture, inject, TestBed, tick } from '@angular/core/testing';
import { async, Observable } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { skip } from 'rxjs/operators';  

import {
    ApiModule,
    Configuration,
    ConfigurationParameters,
    Stamp,
} from '../api/1.0.0'; 

export function apiConfigFactory(): Configuration {
    const params: ConfigurationParameters = {
        basePath: environment.backend_url,
    };
    return new Configuration(params);
}

import { SharedModule } from './shared.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SharedModule', () => {
    let service: SharedModule;
    let fixture: ComponentFixture<Observable<Stamp>>;

    beforeEach(() => {
        TestBed.configureTestingModule({ 
            schemas: [NO_ERRORS_SCHEMA],
            imports:   [ ApiModule.forRoot(apiConfigFactory), HttpClientModule, RouterTestingModule ],
            providers: [ SharedModule ]
        })
        //.compileComponents();
        //service = TestBed.get(SharedModule);
        //fixture = TestBed.inject(service.stamps$)
        service = TestBed.inject(SharedModule);
    });

    it('should initialize Service', () => {
        expect(service).toBeTruthy();
    });

    it('should get 3 Stamps', (done) => {

        // skip first and empty response
        var resultData: Stamp[] = [];
        // skip value from BehaviourSubject
        const skippedOne = service.stamps$.pipe(skip(1)); 
        // fetch list from unit test day, should return 3 records in test database
        service.getStamps("2018-05-05"); 
        skippedOne.subscribe(data => {
            resultData = data;
            expect(resultData.length).toEqual(3); 
            expect(resultData[0].uid).toEqual("DEADBEEF");
            //console.log(data)
            done();
        });
    
    });

});