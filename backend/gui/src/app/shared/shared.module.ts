import { NgModule, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { BrowserModule } from '@angular/platform-browser';

import { environment } from '../../environments/environment';
import { DevelopersService } from '../api/1.0.0/api/developers.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { FormsModule, UntypedFormControl } from '@angular/forms';

import {
  ApiModule,
  Configuration,
  ConfigurationParameters,
  Config, Stamp, Room, Event
} from '../api/1.0.0';

import { ToastComponent } from './toast.component';
import { ErrorDialog } from './error-dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { takeUntil } from 'rxjs/operators';

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: environment.backend_url,
  };
  return new Configuration(params);
}

@NgModule({
  declarations: [
    ToastComponent,
    ErrorDialog
  ],
  imports: [
    CommonModule,

    MatButtonModule,
    MatIconModule,
    MatButtonModule

  ],
  providers: [
    ToastComponent
  ]
})

export class SharedModule implements OnDestroy {

  env = environment;
  public selectedDate: UntypedFormControl = new UntypedFormControl(new Date());

  private stampsSubject = new BehaviorSubject<Stamp[]>([]);
  stamps$: Observable<Stamp[]> = this.stampsSubject.asObservable();

  private roomsSubject = new BehaviorSubject<Room[]>([]);
  rooms$: Observable<Room[]> = this.roomsSubject.asObservable();

  private eventsSubject = new BehaviorSubject<Event[]>([]);
  events$: Observable<Event[]> = this.eventsSubject.asObservable();

    private ngUnsubscribe = new Subject();

  private configSubject = new BehaviorSubject<Config>({
    "config_file": "",
    "port": "",
    "dbfile": "",
    "version": "",
    "application_info": "",
    "application_title": ""
  });
  config$: Observable<Config> = this.configSubject.asObservable();

  public showNav = false;

  constructor(
    public service: DevelopersService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastComponent) {

    /*
     * This is our state control event handler
     *
     * We do observe url changes and set selected state data accordingly.
     * The state change is then triggering the required observables.
     *
     * When an observable item is unselected, it's id is set to -1.
     */
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd))
        return;
      console.log(evt.urlAfterRedirects.substring(1));
      if (evt.urlAfterRedirects.substring(1) == "stamp")
        this.showNav = false;
      else
      this.showNav = true;
    });

    this.getRooms();
    this.getEvents();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getConfig() {
    this.service.configGet().subscribe(res => {
      this.configSubject.next(res)
      //this.appComponent.setTitle(res.application_title ?? "", res.application_info ?? "");
    });
  }

  /**
   * get a stamp list
   */
  getStamps(day: string|null = null) {
    let today = new Date();
    let month = today.getMonth()+1;
    var month_str: string = (month < 10) ? "0"+month.toString() : month.toString();
    var current_day = (today.getDate() > 9) ? today.getDate() : "0" + today.getDate();
    let paramDay: string = (!day) ? today.getFullYear()+'-'+month_str+'-'+current_day : day;
    // reset device ?
    this.service.listJson(paramDay)
      .subscribe(res => {
        this.stampsSubject.next(res)
      });
  }

  /**
   * get a list of rooms
   */
  getRooms() {
    this.service.listRooms()
      .subscribe(res => {
        this.roomsSubject.next(res)
      });
  }

  updateRooms(rooms: Room[]) {
    this.service.insertRooms(rooms)
      .subscribe(res => {
        this.getRooms();
      }, error => {
        console.log("Error updating Rooms");
        console.log(error);
        this.toast.openSnackBar(error.error.message);
        this.getRooms();
      });
  }

  getRoomCount() {
    return this.roomsSubject.getValue().length
  }

  /**
   * get a list of events
   */
  getEvents() {
    this.service.listEvents()
      .subscribe(res => {
        this.eventsSubject.next(res)
      });
  }

  deleteRoom(roomId: number) {
    this.service.deleteRoom(roomId)
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(res => {
      this.toast.openSnackBar("Room Deleted");
      this.getRooms();
    });

  }



}
