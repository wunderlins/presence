import {Component, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErroMessage } from './error-message'

@Component({
    selector: 'error-dialog',
    template: `
        <h1>Network error</h1>

        <!-- net::ERR_CONNECTION_REFUSED -->
        <div *ngIf="!data.res.status">
            Failed to connect to the server, check your network.
        </div>

        <div *ngIf="data.res.status">
            {{data.res.error.code}} {{data.res.error.message}}
        </div>
        <br>
        <button mat-flat-button color="primary" (click)="onNoClick()">Ok</button>
    `,
})
export class ErrorDialog {

    constructor(
        public dialogRef: MatDialogRef<ErrorDialog>,
        @Inject(MAT_DIALOG_DATA) public data: ErroMessage) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

}
